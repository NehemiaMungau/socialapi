﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Web.Http;

namespace SocialApi.Controllers
{
    public class LikesController : ApiController
    {
        private readonly IChannelService _channelService;

        private ServiceHeader serviceHeader;

        public LikesController(IChannelService _channelService)
        {
            serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            serviceHeader.ApplicationDomainName = "DEV_FSA";
        }

        public async Task<HttpResponseMessage> Post(LikeDTO likeDTO)
        {
            var response = new HttpResponseMessage();

            try
            {

            }
            catch (Exception ex)
            {
                var error = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error has occurred."),
                    ReasonPhrase = "Failed."
                };

               
                throw new HttpResponseException(error);
            }

        }
    }
}
