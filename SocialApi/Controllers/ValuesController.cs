﻿using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Web.Http;
using System.Web.Http.Description;

namespace SocialApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ValuesController : ApiController
    {
        private readonly IChannelService _channelService;

        private ServiceHeader serviceHeader;


        public ValuesController(IChannelService channelService)
        {
            _channelService = channelService ?? throw new ArgumentNullException(nameof(channelService));


            serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            serviceHeader.ApplicationDomainName = "DEV_FSA"; ;

        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
