﻿using Infrastructure.Crosscutting.Framework.Logging;
using Microsoft.Practices.Unity;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;

namespace School.WindowsService
{
    public class QuartzUnityExtension: UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<IJobFactory, UnityJobFactory>(new ContainerControlledLifetimeManager());

            Container.RegisterType<ISchedulerFactory, UnitySchedulerFactory>(new ContainerControlledLifetimeManager());

           // Container.RegisterType<ILogger, SerilogLogger>(new ContainerControlledLifetimeManager());
        }
    }
}
