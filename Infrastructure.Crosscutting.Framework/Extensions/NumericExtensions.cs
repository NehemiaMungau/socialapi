﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Crosscutting.Framework.Extensions
{
    public static class NumericExtensions
    {

#if !SILVERLIGHT

        public static decimal RoundUp(this decimal amount)
        {
            var result = Math.Round(amount, 4, MidpointRounding.AwayFromZero);

            result = Math.Ceiling(result);

            return result;
        }

        /// <summary>
        /// Searches in the haystack array for the given needle using the default equality operator and returns the index at which the needle starts.
        /// </summary>
        /// <typeparam name="T">Type of the arrays.</typeparam>
        /// <param name="haystack">Sequence to operate on.</param>
        /// <param name="needle">Sequence to search for.</param>
        /// <returns>Index of the needle within the haystack or -1 if the needle isn't contained.</returns>
        public static IEnumerable<int> IndexOf<T>(this T[] haystack, T[] needle)
        {
            if ((needle != null) && (haystack.Length >= needle.Length))
            {
                for (int l = 0; l < haystack.Length - needle.Length + 1; l++)
                {
                    if (!needle.Where((data, index) => !haystack[l + index].Equals(data)).Any())
                    {
                        yield return l;
                    }
                }
            }
        }

        //public static unsafe long IndexOf(this byte[] Haystack, byte[] Needle)
        //{
        //    fixed (byte* H = Haystack) fixed (byte* N = Needle)
        //    {
        //        long i = 0;
        //        for (byte* hNext = H, hEnd = H + Haystack.LongLength; hNext < hEnd; i++, hNext++)
        //        {
        //            bool Found = true;
        //            for (byte* hInc = hNext, nInc = N, nEnd = N + Needle.LongLength; Found && nInc < nEnd; Found = *nInc == *hInc, nInc++, hInc++) ;
        //            if (Found) return i;
        //        }
        //        return -1;
        //    }
        //}

        //public static unsafe List<long> IndexesOf(this byte[] Haystack, byte[] Needle)
        //{
        //    List<long> Indexes = new List<long>();
        //    fixed (byte* H = Haystack) fixed (byte* N = Needle)
        //    {
        //        long i = 0;
        //        for (byte* hNext = H, hEnd = H + Haystack.LongLength; hNext < hEnd; i++, hNext++)
        //        {
        //            bool Found = true;
        //            for (byte* hInc = hNext, nInc = N, nEnd = N + Needle.LongLength; Found && nInc < nEnd; Found = *nInc == *hInc, nInc++, hInc++) ;
        //            if (Found) Indexes.Add(i);
        //        }
        //        return Indexes;
        //    }
        //}

#endif

        public static int FirstDigit(this int value)
        {
            int i = Math.Abs(value);

            while (i >= 10)
            {
                i /= 10;
            }

            return i;
        }
    }
}
