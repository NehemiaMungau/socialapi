﻿using System;
using System.ComponentModel;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public enum Gender
    {
        [Description("Male")]
        Male = 0,
        [Description("Female")]
        Female = 1
    }
    public enum AuditLogEventType
    {
        [Description("Entity_Added")]
        Entity_Added = 0,
        [Description("Entity_Modified")]
        Entity_Modified = 1,
        [Description("Entity_Deleted")]
        Entity_Deleted = 2,
        [Description("Sys_Login")]
        Sys_Login = 3,
        [Description("Sys_Logout")]
        Sys_Logout = 4,
        [Description("Sys_Other")]
        Sys_Other = 5
    }

    [Flags]
    public enum DLRStatus
    {
        [Description("UnKnown")]
        UnKnown = 1,
        [Description("Failed")]
        Failed = 2,
        [Description("Pending")]
        Pending = 4,
        [Description("Delivered")]
        Delivered = 8,
        [Description("Not Applicable")]
        NotApplicable = 16,
        [Description("Submitted")]
        Submitted = 32,
    }

    [Flags]
    public enum MessageOrigin
    {
        [Description("Internal")]
        Within = 1,
        [Description("External")]
        Without = 2,
        [Description("Other")]
        Other = 4
    }

    public enum QueuePriority
    {
        [Description("Lowest")]
        Lowest = 0,
        [Description("Very Low")]
        VeryLow = 1,
        [Description("Low")]
        Low = 2,
        [Description("Normal")]
        Normal = 3,
        [Description("Above Normal")]
        AboveNormal = 4,
        [Description("High")]
        High = 5,
        [Description("Very High")]
        VeryHigh = 6,
        [Description("Highest")]
        Highest = 7,
    }

    public enum MessageCategory
    {
        [Description("SMS Alert")]
        SMSAlert = 0,
        [Description("E-mail Alert")]
        EmailAlert = 1,
        [Description("Plugin Alert")]
        PluginAlert = 2,
    }

    public enum CustomerDocumentType
    {
        [Description("Passport")]
        Passport = 0,
        [Description("National ID")]
        ID = 1,
        [Description("Tax PIN")]
        TaxPIN = 2,
        [Description("Signature")]
        Signature = 3,
        [Description("Driving License")]
        DrivingLicense = 4,
        [Description("Alien ID")]
        AlienID = 5
    }

    public enum RecordStatus
    {
        [Description("Approved")]
        Approved,
        [Description("New")]
        New,
        [Description("Edited")]
        Edited,
        [Description("Rejected")]
        Rejected,
        [Description("Extracted")]
        Extracted,
    }

    public enum Title
    {
        [Description("Mr")]
        Mr = 1,
        [Description("Mrs")]
        Mrs = 2,
        [Description("Ms")]
        Ms = 3,
        [Description("Dr")]
        Dr = 4,
        [Description("Prof")]
        Prof = 5
    }

    public enum IdType
    {
        [Description("National ID")]
        NationalID = 1,
        [Description("Passport")]
        Passport = 2,
        [Description("Alien ID")]
        AlienID = 3,
        [Description("Driving License")]
        DrivingLicense = 4
    }

    public enum MaritalStatus
    {
        [Description("Single")]
        Single = 1,
        [Description("Married")]
        Married = 2,
        [Description("Widowed")]
        Widowed = 3
    }

    public enum ResidenceType
    {
        [Description("Rented")]
        Rented = 1,
        [Description("Owned")]
        Owned = 2,
        [Description("Lease")]
        Lease = 3,
        [Description("Family")]
        Family = 4
    }

    public enum EmploymentStatus
    {
        [Description("Permanent")]
        Permanent = 1,
        [Description("PartTime")]
        PartTime = 2,
        [Description("Contract")]
        Contract = 3
    }

    public enum Relationship
    {
        [Description("Father")]
        Father = 1,
        [Description("Mother")]
        Mother = 2,
        [Description("Son")]
        Son = 3,
        [Description("Daughter")]
        Daughter = 4,
        [Description("Brother")]
        Brother = 5,
        [Description("Sister")]
        Sister = 6,
        [Description("Other")]
        Other = 7
    }

    public enum IncomeRange
    {
        [Description("999 - 10000")]
        From999to10000 = 1,
        [Description("10001 - 20000")]
        From10001to20000 = 2,
        [Description("20001 - 40000")]
        From20001to40000 = 3,
        [Description("400001 - 80000")]
        From400001to80000 = 4,
        [Description("80001 and above")]
        From80001andabove = 5
    }

}
