﻿using System;
using System.Xml.Serialization;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    [Serializable]
    public class AuditInfo
    {
        [XmlAttribute]
        public string ColumnName { get; set; }

        [XmlAttribute]
        public string OriginalValue { get; set; }

        [XmlAttribute]
        public string NewValue { get; set; }
    }
}
