﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public sealed class DefaultSettings
    {
        private static readonly object SyncRoot = new object();

        private DefaultSettings() { }

        private static DefaultSettings instance;
        public static DefaultSettings Instance
        {
            get
            {
                lock (SyncRoot)
                {
                    if (instance == null)
                        instance = new DefaultSettings();

                    /*
                     * Only vendor should know this :-(
                     */
                    instance.RootUser = "";
                    instance.RootPassword = "";
                    instance.AuditUser = "";
                    instance.AuditPassword = "";
                    instance.Password = "";
                    instance.PasswordQuestion = "Where were you when you first heard about 9/11?";
                    instance.PasswordAnswer = "";
                    instance.RootEmail = "";
                    instance.TablePrefix = "Social_";
                    instance.ApplicationDisplayName = "Social_' FSA ™";
                    instance.ApplicationCopyright = string.Format("Copyright © 2012-2017, Social' FSA® and/or its affiliates.{0}All rights reserved.", Environment.NewLine);

                    instance.PageSizes = new List<int> { 15, 25, 50, 100, 200, 400 };
                    
                    instance.EndDateTimeSpan = new TimeSpan(23, 59, 59);
                }

                return instance;
            }
        }

        public string RootUser { get; private set; }

        public string RootPassword { get; private set; }

        public string AuditUser { get; private set; }

        public string AuditPassword { get; private set; }

        public string Password { get; private set; }

        public string PasswordQuestion { get; private set; }

        public string PasswordAnswer { get; private set; }

        public string RootEmail { get; private set; }

        public string ApplicationDisplayName { get; private set; }

        public string ApplicationCopyright { get; private set; }

        public string TablePrefix { get; private set; }

        public TimeSpan EndDateTimeSpan { get; private set; }

        public List<int> PageSizes { get; private set; }
    }
}
