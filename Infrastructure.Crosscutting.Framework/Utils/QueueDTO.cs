﻿using System;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    [Serializable]
    public class QueueDTO
    {
        public Guid RecordId { get; set; }

        public string AppDomainName { get; set; }

        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        public bool SmtpEnableSsl { get; set; }

        public string SmtpUsername { get; set; }

        public string SmtpPassword { get; set; }

		public string BulkTextUrl { get; set; }

		public string BulkTextUsername { get; set; }

		public string BulkTextPassword { get; set; }

		public string BulkTextSenderId { get; set; }
	}
}
