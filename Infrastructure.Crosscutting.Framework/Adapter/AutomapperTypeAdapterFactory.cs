﻿using AutoMapper;
using System;
using System.Linq;

namespace Infrastructure.Crosscutting.Framework.Adapter
{
    public class AutomapperTypeAdapterFactory : ITypeAdapterFactory
    {
        #region Constructor

        public AutomapperTypeAdapterFactory()
        {
            /// <summary>
            /// Enumerates over all relevant assemblies, scanning each for non-abstract implementations of
            /// AutoMapper.Profile. For each match that is found, a new instance is constructed and added
            /// to the AutoMapper configuration.
            /// <summary>

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (var assembly in assemblies)
            {
                if (assembly.FullName.StartsWith("Application.MainBoundedContext.DTO", StringComparison.OrdinalIgnoreCase))
                {
                    var profiles = assembly.GetTypes().Where(t => t != typeof(Profile) && typeof(Profile).IsAssignableFrom(t) && !t.IsAbstract);

                    Mapper.Initialize(cfg =>
                    {
                        foreach (var profile in profiles)
                        {
                            cfg.AddProfile(Activator.CreateInstance(profile) as Profile);
                        }
                    });
                }
            }
        } 

        #endregion

        #region ITypeAdapterFactory Members
        
        public ITypeAdapter Create()
        {
            return new AutomapperTypeAdapter();
        } 

        #endregion
    }
}
