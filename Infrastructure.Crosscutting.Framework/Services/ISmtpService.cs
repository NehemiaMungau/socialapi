﻿using System.Net.Mail;

namespace Infrastructure.Crosscutting.Framework.Services
{
    public interface ISmtpService
    {
        void AddAttachment(string filePath);

        void SendEmail(string host, int port, bool enableSsl, string userName, string password, MailMessage mailMessage);

        void SendEmail(string host, int port, bool enableSsl, string userName, string password, MailAddress from, MailAddressCollection to, string subject, string body, bool isBodyHtml);

        void SendEmail(string host, int port, bool enableSsl, string userName, string password, string from, string to, string subject, string body, bool isBodyHtml);

        void SendEmail(string host, int port, bool enableSsl, string userName, string password, MailAddress from, MailAddressCollection to, MailAddressCollection cc, string subject, string body, bool isBodyHtml);

        void SendEmail(string host, int port, bool enableSsl, string userName, string password, string from, string to, string cc, string subject, string body, bool isBodyHtml);
    }
}
