﻿using Infrastructure.Crosscutting.Framework.Utils;
using System.Messaging;

namespace Infrastructure.Crosscutting.Framework.Services
{
    public interface IMessageQueueService
    {
        void Send(string queuePath, object data, MessageCategory messageCategory, MessagePriority priority, int timeToBeReceived);
    }
}
