﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.ComponentModel.Composition;
using System.Messaging;

namespace Infrastructure.Crosscutting.Framework.Services
{
    [Export(typeof(IMessageQueueService))]
    public class MessageQueueService : IMessageQueueService
    {
        public void Send(string queuePath, object data, MessageCategory messageCategory, MessagePriority priority, int timeToBeReceived)
        {
            using (MessageQueue messageQueue = new MessageQueue(queuePath, QueueAccessMode.Send))
            {
                messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                messageQueue.MessageReadPropertyFilter.Priority = true;

                messageQueue.Formatter = new BinaryMessageFormatter();

                using (Message message = new Message(data, new BinaryMessageFormatter()))
                {
                    message.Label = string.Format("{0}|{1}", EnumHelper.GetDescription(messageCategory), EnumHelper.GetDescription(priority));
                    message.AppSpecific = (int)messageCategory;
                    message.Priority = priority;
                    message.TimeToBeReceived = TimeSpan.FromMinutes(timeToBeReceived);

                    messageQueue.Send(message);
                }
            }
        }
    }
}
