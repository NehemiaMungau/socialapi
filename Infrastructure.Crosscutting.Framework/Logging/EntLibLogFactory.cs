﻿namespace Infrastructure.Crosscutting.Framework.Logging
{
    public class EntLibLogFactory : ILoggerFactory
    {
        public ILogger Create()
        {
            return new EntLibLog();
        }
    }
}
