﻿using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
/* 
 * Copyright (C) 2014 Mehdi El Gueddari
 * http://mehdi.me
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Numero3.EntityFramework.Implementation
{
    /// <summary>
    /// As its name suggests, DbContextCollection maintains a collection of DbContext instances.
    /// 
    /// What it does in a nutshell:
    /// - Lazily instantiates DbContext instances when its Get Of TDbContext () method is called
    /// (and optionally starts an explicit database transaction).
    /// - Keeps track of the DbContext instances it created so that it can return the existing
    /// instance when asked for a DbContext of a specific type.
    /// - Takes care of committing / rolling back changes and transactions on all the DbContext
    /// instances it created when its Commit() or Rollback() method is called.
    /// 
    /// </summary>
    public class DbContextCollection : IDbContextCollection
    {
        private Dictionary<Type, DbContext> _initializedDbContexts;
        private Dictionary<DbContext, DbContextTransaction> _transactions;
        private IsolationLevel? _isolationLevel;
        private readonly IDbContextFactory _dbContextFactory;
        private bool _disposed;
        private bool _completed;
        private bool _readOnly;

        internal Dictionary<Type, DbContext> InitializedDbContexts { get { return _initializedDbContexts; } }

        public DbContextCollection(bool readOnly = false, IsolationLevel? isolationLevel = null, IDbContextFactory dbContextFactory = null)
        {
            _disposed = false;
            _completed = false;

            _initializedDbContexts = new Dictionary<Type, DbContext>();
            _transactions = new Dictionary<DbContext, DbContextTransaction>();

            _readOnly = readOnly;
            _isolationLevel = isolationLevel;
            _dbContextFactory = dbContextFactory;
        }

        public TDbContext Get<TDbContext>(ServiceHeader serviceHeader) where TDbContext : DbContext
        {
            if (_disposed)
                throw new ObjectDisposedException("DbContextCollection");

            var requestedType = typeof(TDbContext);

            if (!_initializedDbContexts.ContainsKey(requestedType))
            {
                // First time we've been asked for this particular DbContext type.
                // Create one, cache it and start its database transaction if needed.
                var dbContext = _dbContextFactory != null
                    ? _dbContextFactory.CreateDbContext<TDbContext>(serviceHeader)
                    : Activator.CreateInstance<TDbContext>();

                _initializedDbContexts.Add(requestedType, dbContext);

                if (_readOnly)
                {
                    dbContext.Configuration.AutoDetectChangesEnabled = false;
                }

                if (_isolationLevel.HasValue)
                {
                    var tran = dbContext.Database.BeginTransaction(_isolationLevel.Value);
                    _transactions.Add(dbContext, tran);
                }
            }

            return _initializedDbContexts[requestedType] as TDbContext;
        }

        public int Commit(ServiceHeader serviceHeader)
        {
            if (_disposed)
                throw new ObjectDisposedException("DbContextCollection");
            if (_completed)
                throw new InvalidOperationException("You can't call Commit() or Rollback() more than once on a DbContextCollection. All the changes in the DbContext instances managed by this collection have already been saved or rollback and all database transactions have been completed and closed. If you wish to make more data changes, create a new DbContextCollection and make your changes there.");

            // Best effort. You'll note that we're not actually implementing an atomic commit 
            // here. It entirely possible that one DbContext instance will be committed successfully
            // and another will fail. Implementing an atomic commit would require us to wrap
            // all of this in a TransactionScope. The problem with TransactionScope is that 
            // the database transaction it creates may be automatically promoted to a 
            // distributed transaction if our DbContext instances happen to be using different 
            // databases. And that would require the DTC service (Distributed Transaction Coordinator)
            // to be enabled on all of our live and dev servers as well as on all of our dev workstations.
            // Otherwise the whole thing would blow up at runtime. 

            // In practice, if our services are implemented following a reasonably DDD approach,
            // a business transaction (i.e. a service method) should only modify entities in a single
            // DbContext. So we should never find ourselves in a situation where two DbContext instances
            // contain uncommitted changes here. We should therefore never be in a situation where the below
            // would result in a partial commit. 

            ExceptionDispatchInfo lastError = null;

            var c = 0;

            foreach (var dbContext in _initializedDbContexts.Values)
            {
                try
                {
                    if (!_readOnly)
                    {
                        List<AuditLogBulkCopy> bulkCopyList = new List<AuditLogBulkCopy>();

                        // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
                        foreach (var entry in dbContext.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                        {
                            // For each changed record, get the audit record entries and add them
                            var wrapper = GetAuditRecordsForChange(dbContext, entry);

                            if (wrapper != null)
                            {
                                bulkCopyList.Add(wrapper.GetBulkCopyEntry(serviceHeader));
                            }
                        }

                        if (bulkCopyList.Any())
                        {
                            if (serviceHeader != null && !string.IsNullOrWhiteSpace(serviceHeader.ApplicationUserName) && serviceHeader.ApplicationUserName.Equals("auditor", StringComparison.OrdinalIgnoreCase))
                                throw new InvalidOperationException("Sorry, but CRUD operations are not permitted!");

                            BulkInsert<AuditLogBulkCopy>(dbContext.Database.Connection.ConnectionString, bulkCopyList);
                        }

                        c += dbContext.SaveChanges();
                    }

                    // If we've started an explicit database transaction, time to commit it now.
                    var tran = GetValueOrDefault(_transactions, dbContext);
                    if (tran != null)
                    {
                        tran.Commit();
                        tran.Dispose();
                    }
                }
                catch (Exception e)
                {
                    lastError = ExceptionDispatchInfo.Capture(e);
                }
            }

            _transactions.Clear();
            _completed = true;

            if (lastError != null)
                lastError.Throw(); // Re-throw while maintaining the exception's original stack track

            return c;
        }

        public Task<int> CommitAsync(ServiceHeader serviceHeader)
        {
            return CommitAsync(serviceHeader, CancellationToken.None);
        }

        public async Task<int> CommitAsync(ServiceHeader serviceHeader, CancellationToken cancelToken)
        {
            if (cancelToken == null)
                throw new ArgumentNullException("cancelToken");
            if (_disposed)
                throw new ObjectDisposedException("DbContextCollection");
            if (_completed)
                throw new InvalidOperationException("You can't call Commit() or Rollback() more than once on a DbContextCollection. All the changes in the DbContext instances managed by this collection have already been saved or rollback and all database transactions have been completed and closed. If you wish to make more data changes, create a new DbContextCollection and make your changes there.");

            // See comments in the sync version of this method for more details.

            ExceptionDispatchInfo lastError = null;

            var c = 0;

            foreach (var dbContext in _initializedDbContexts.Values)
            {
                try
                {
                    if (!_readOnly)
                    {
                        List<AuditLogBulkCopy> bulkCopyList = new List<AuditLogBulkCopy>();

                        // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
                        foreach (var entry in dbContext.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                        {
                            // For each changed record, get the audit record entries and add them
                            var wrapper = GetAuditRecordsForChange(dbContext, entry);

                            if (wrapper != null)
                            {
                                bulkCopyList.Add(wrapper.GetBulkCopyEntry(serviceHeader));
                            }
                        }

                        if (bulkCopyList.Any())
                        {
                            if (serviceHeader != null && !string.IsNullOrWhiteSpace(serviceHeader.ApplicationUserName) && serviceHeader.ApplicationUserName.Equals("auditor", StringComparison.OrdinalIgnoreCase))
                                throw new InvalidOperationException("Sorry, but CRUD operations are not permitted!");

                            BulkInsert<AuditLogBulkCopy>(dbContext.Database.Connection.ConnectionString, bulkCopyList);
                        }

                        c += await dbContext.SaveChangesAsync(cancelToken).ConfigureAwait(false);
                    }

                    // If we've started an explicit database transaction, time to commit it now.
                    var tran = GetValueOrDefault(_transactions, dbContext);
                    if (tran != null)
                    {
                        tran.Commit();
                        tran.Dispose();
                    }
                }
                catch (Exception e)
                {
                    lastError = ExceptionDispatchInfo.Capture(e);
                }
            }

            _transactions.Clear();
            _completed = true;

            if (lastError != null)
                lastError.Throw(); // Re-throw while maintaining the exception's original stack track

            return c;
        }

        public void Rollback()
        {
            if (_disposed)
                throw new ObjectDisposedException("DbContextCollection");
            if (_completed)
                throw new InvalidOperationException("You can't call Commit() or Rollback() more than once on a DbContextCollection. All the changes in the DbContext instances managed by this collection have already been saved or rollback and all database transactions have been completed and closed. If you wish to make more data changes, create a new DbContextCollection and make your changes there.");

            ExceptionDispatchInfo lastError = null;

            foreach (var dbContext in _initializedDbContexts.Values)
            {
                // There's no need to explicitly rollback changes in a DbContext as
                // DbContext doesn't save any changes until its SaveChanges() method is called.
                // So "rolling back" for a DbContext simply means not calling its SaveChanges()
                // method. 

                // But if we've started an explicit database transaction, then we must roll it back.
                var tran = GetValueOrDefault(_transactions, dbContext);
                if (tran != null)
                {
                    try
                    {
                        tran.Rollback();
                        tran.Dispose();
                    }
                    catch (Exception e)
                    {
                        lastError = ExceptionDispatchInfo.Capture(e);
                    }
                }
            }

            _transactions.Clear();
            _completed = true;

            if (lastError != null)
                lastError.Throw(); // Re-throw while maintaining the exception's original stack track
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            // Do our best here to dispose as much as we can even if we get errors along the way.
            // Now is not the time to throw. Correctly implemented applications will have called
            // either Commit() or Rollback() first and would have got the error there.

            if (!_completed)
            {
                try
                {
                    if (_readOnly) Commit(null);
                    else Rollback();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e);
                }
            }

            foreach (var dbContext in _initializedDbContexts.Values)
            {
                try
                {
                    dbContext.Dispose();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e);
                }
            }

            _initializedDbContexts.Clear();
            _disposed = true;
        }

        /// <summary>
        /// Returns the value associated with the specified key or the default 
        /// value for the TValue  type.
        /// </summary>
        private static TValue GetValueOrDefault<TKey, TValue>(IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : default(TValue);
        }

        private static AuditInfoWrapper GetAuditRecordsForChange(DbContext dbContext, DbEntityEntry dbEntry)
        {
            // Get table name 
            string tableName = string.Format("{0}{1}", DefaultSettings.Instance.TablePrefix, dbContext.GetTableName(dbEntry));

            if (tableName.Equals(string.Format("{0}AuditLogs", DefaultSettings.Instance.TablePrefix), StringComparison.OrdinalIgnoreCase))
                return null;

            var entityType = dbEntry.Entity.GetType();

            if (entityType.BaseType != null && entityType.Namespace == "System.Data.Entity.DynamicProxies")
                entityType = entityType.BaseType;

            // Get primary key value (If you have more than one key column, this will need to be adjusted)
            string keyName = entityType.GetProperties().Single(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).Name;

            List<AuditInfo> infoList = new List<AuditInfo>();

            byte eventType = 0;

            string recordID = string.Empty;

            if (dbEntry.State == EntityState.Added)
            {
                foreach (string propertyName in dbEntry.CurrentValues.PropertyNames)
                {
                    var currentValues = string.Empty;
                    if (dbEntry.CurrentValues.GetValue<object>(propertyName) != null)
                    {
                        if (dbEntry.CurrentValues.GetValue<object>(propertyName) is DbPropertyValues)
                            currentValues = ((DbPropertyValues)dbEntry.CurrentValues.GetValue<object>(propertyName)).GetValues();
                        else currentValues = dbEntry.CurrentValues.GetValue<object>(propertyName).ToString();
                    }

                    eventType = (byte)AuditLogEventType.Entity_Added;

                    recordID = dbEntry.CurrentValues.GetValue<object>(keyName).ToString();

                    var addedAuditLog = new AuditInfo
                    {
                        ColumnName = propertyName,
                        OriginalValue = string.Empty,
                        NewValue = currentValues,
                    };

                    infoList.Add(addedAuditLog);
                }
            }
            else if (dbEntry.State == EntityState.Deleted)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    var originalValues = string.Empty;
                    if (dbEntry.OriginalValues.GetValue<object>(propertyName) != null)
                    {
                        if (dbEntry.OriginalValues.GetValue<object>(propertyName) is DbPropertyValues)
                            originalValues = ((DbPropertyValues)dbEntry.OriginalValues.GetValue<object>(propertyName)).GetValues();
                        else originalValues = dbEntry.OriginalValues.GetValue<object>(propertyName).ToString();
                    }

                    eventType = (byte)AuditLogEventType.Entity_Deleted;

                    recordID = dbEntry.OriginalValues.GetValue<object>(keyName).ToString();

                    var deletedAuditLog = new AuditInfo
                    {
                        ColumnName = propertyName,
                        OriginalValue = originalValues,
                        NewValue = string.Empty,
                    };

                    infoList.Add(deletedAuditLog);
                }
            }
            else if (dbEntry.State == EntityState.Modified)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    // For updates, we only want to capture the columns that actually changed
                    if (!object.Equals(dbEntry.OriginalValues.GetValue<object>(propertyName), dbEntry.CurrentValues.GetValue<object>(propertyName)))
                    {
                        var originalValues = string.Empty;
                        if (dbEntry.OriginalValues.GetValue<object>(propertyName) != null)
                        {
                            if (dbEntry.OriginalValues.GetValue<object>(propertyName) is DbPropertyValues)
                                originalValues = ((DbPropertyValues)dbEntry.OriginalValues.GetValue<object>(propertyName)).GetValues();
                            else originalValues = dbEntry.OriginalValues.GetValue<object>(propertyName).ToString();
                        }

                        var currentValues = string.Empty;
                        if (dbEntry.CurrentValues.GetValue<object>(propertyName) != null)
                        {
                            if (dbEntry.CurrentValues.GetValue<object>(propertyName) is DbPropertyValues)
                                currentValues = ((DbPropertyValues)dbEntry.CurrentValues.GetValue<object>(propertyName)).GetValues();
                            else currentValues = dbEntry.CurrentValues.GetValue<object>(propertyName).ToString();
                        }

                        eventType = (byte)AuditLogEventType.Entity_Modified;

                        recordID = dbEntry.OriginalValues.GetValue<object>(keyName).ToString();

                        var modifiedAuditLog = new AuditInfo
                        {
                            ColumnName = propertyName,
                            OriginalValue = originalValues,
                            NewValue = currentValues,
                        };

                        infoList.Add(modifiedAuditLog);
                    }
                }
            }
            // Otherwise, don't do anything, we don't care about Unchanged or Detached entities

            return new AuditInfoWrapper { TableName = tableName, EventType = eventType, RecordID = recordID, AuditInfoCollection = infoList };
        }

        private static bool BulkInsert<T>(string connectionString, IList<T> list)
        {
            var result = default(bool);

            using (var bulkCopy = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.FireTriggers))
            {
                bulkCopy.BulkCopyTimeout = 300;
                bulkCopy.BatchSize = 5000;
                bulkCopy.DestinationTableName = string.Format("{0}AuditLogs", DefaultSettings.Instance.TablePrefix);

                var table = new DataTable();

                var props = TypeDescriptor.GetProperties(typeof(T))
                                           //Dirty hack to make sure we only have system data types 
                                           //i.e. filter out the relationships/collections
                                           .Cast<PropertyDescriptor>()
                                           .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
                                           .ToArray();

                foreach (var propertyInfo in props)
                {
                    bulkCopy.ColumnMappings.Add(propertyInfo.Name, propertyInfo.Name);

                    table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
                }

                var values = new object[props.Length];

                foreach (var item in list)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item);
                    }

                    table.Rows.Add(values);
                }

                bulkCopy.WriteToServer(table);

                result = true;
            }

            return result;
        }
    }

    public class AuditLogBulkCopy
    {
        public Guid Id { get; set; }

        public byte EventType { get; set; }

        public string TableName { get; set; }

        public string RecordID { get; set; }

        public string AdditionalNarration { get; set; }

        public string ApplicationUserName { get; set; }

        public string EnvironmentUserName { get; set; }

        public string EnvironmentMachineName { get; set; }

        public string EnvironmentDomainName { get; set; }

        public string EnvironmentOSVersion { get; set; }

        public string EnvironmentMACAddress { get; set; }

        public string EnvironmentMotherboardSerialNumber { get; set; }

        public string EnvironmentProcessorId { get; set; }

        public string EnvironmentIPAddress { get; set; }

        public DateTime EventDate { get; set; }
    }

    internal static class Extensions
    {
        public static string GetTableName<T>(this DbContext context) where T : class
        {
            var workspace = ((IObjectContextAdapter)context).ObjectContext.MetadataWorkspace;

            var mappingItemCollection = (StorageMappingItemCollection)workspace.GetItemCollection(DataSpace.CSSpace);

            var storeContainer = ((EntityContainerMapping)mappingItemCollection[0]).StoreEntityContainer;

            var baseEntitySet = storeContainer.BaseEntitySets.Single(es => es.Name == typeof(T).Name);

            return string.Format("{0}.{1}", baseEntitySet.Schema, baseEntitySet.Table);
        }

        public static string GetTableName(this DbContext context, DbEntityEntry ent)
        {
            var objectContext = ((IObjectContextAdapter)context).ObjectContext;

            var entityType = ent.Entity.GetType();

            if (entityType.BaseType != null && entityType.Namespace == "System.Data.Entity.DynamicProxies")
                entityType = entityType.BaseType;

            string entityTypeName = entityType.Name;

            var container = objectContext.MetadataWorkspace.GetEntityContainer(objectContext.DefaultContainerName, DataSpace.CSpace);

            string entitySetName = (from meta in container.BaseEntitySets
                                    where meta.ElementType.Name == entityTypeName
                                    select meta.Name).First();
            return entitySetName;
        }

        public static string GetValues(this DbPropertyValues values)
        {
            var sb = new StringBuilder();

            foreach (var propertyName in values.PropertyNames)
            {
                sb.AppendLine(string.Format("Property '{0}' has value '{1}'", propertyName, values[propertyName]));
            }

            return string.Format("{0}", sb);
        }

        public static string GetXml(this AuditInfoWrapper value)
        {
            var xmlMessage = string.Empty;

            var utf8 = new UTF8Encoding();

            using (var stream = new MemoryStream())
            {
                using (var xtWriter = new XmlTextWriter(stream, utf8))
                {
                    var serializer = new XmlSerializer(typeof(AuditInfoWrapper));

                    serializer.Serialize(xtWriter, value);

                    xtWriter.Flush();

                    stream.Seek(0, System.IO.SeekOrigin.Begin);

                    xmlMessage = utf8.GetString(stream.ToArray());
                }
            }

            return xmlMessage;
        }

        public static AuditLogBulkCopy GetBulkCopyEntry(this AuditInfoWrapper value, ServiceHeader serviceHeader)
        {
            return new AuditLogBulkCopy
            {
                Id = IdentityGenerator.NewSequentialGuid(),
                TableName = value.TableName,
                EventType = value.EventType,
                RecordID = value.RecordID,
                AdditionalNarration = value.GetXml(),
                ApplicationUserName = serviceHeader != null ? serviceHeader.ApplicationUserName : string.Empty,
                EnvironmentUserName = serviceHeader != null ? serviceHeader.EnvironmentUserName : string.Empty,
                EnvironmentMachineName = serviceHeader != null ? serviceHeader.EnvironmentMachineName : string.Empty,
                EnvironmentDomainName = serviceHeader != null ? serviceHeader.EnvironmentDomainName : string.Empty,
                EnvironmentOSVersion = serviceHeader != null ? serviceHeader.EnvironmentOSVersion : string.Empty,
                EnvironmentMACAddress = serviceHeader != null ? serviceHeader.EnvironmentMACAddress : string.Empty,
                EnvironmentMotherboardSerialNumber = serviceHeader != null ? serviceHeader.EnvironmentMotherboardSerialNumber : string.Empty,
                EnvironmentProcessorId = serviceHeader != null ? serviceHeader.EnvironmentProcessorId : string.Empty,
                EnvironmentIPAddress = serviceHeader != null ? serviceHeader.EnvironmentIPAddress : string.Empty,
                EventDate = DateTime.Now,
            };
        }
    }
}