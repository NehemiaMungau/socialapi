﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.Services;
using Application.Seedwork;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using DistributedServices.Seedwork.ErrorHandlers;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Extensions;
using LazyCache;
using Microsoft.Practices.Unity.Utility;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandlerAttribute()]
    [UnityInstanceProviderServiceBehavior()]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class UtilityService : IUtilityService
    {
        private readonly IEnumerationAppService _enumerationAppService;
        private readonly IAppCache _appCache;
        private readonly IDbContextFactory _dbContextFactory;

        public UtilityService(
            IEnumerationAppService enumerationAppService,
            IAppCache appCache,
            IDbContextFactory dbContextFactory)
        {
            Guard.ArgumentNotNull(enumerationAppService, "enumerationAppService");
            Guard.ArgumentNotNull(appCache, "appCache");
            Guard.ArgumentNotNull(dbContextFactory, "dbContextFactory");

            _enumerationAppService = enumerationAppService;
            _appCache = appCache;
            _dbContextFactory = dbContextFactory;
        }

        public PageCollectionInfo<ApplicationDomainWrapper> FindApplicationDomainsByFilterInPage(string text, int pageIndex, int pageSize)
        {
            var applicationDomains = GetCachedApplicationDomains();

            if (applicationDomains != null && applicationDomains.Any())
            {
                var sortFields = new List<string> { "DomainName" };

                var applicationDomainPagedCollection = ProjectionsExtensionMethods.AllMatchingPaged(applicationDomains.AsQueryable(), ApplicationDomainWrapperSpecifications.DefaultSpec(text), pageIndex, pageSize, sortFields, true);

                if (applicationDomainPagedCollection != null)
                {
                    return new PageCollectionInfo<ApplicationDomainWrapper>
                    {
                        PageCollection = new List<ApplicationDomainWrapper>(applicationDomainPagedCollection.Items),
                        ItemsCount = applicationDomainPagedCollection.TotalItems
                    };
                }
                else return null;
            }
            else return null;
        }

        public bool ConfigureApplicationDatabase()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            var autoConfiguration = new Infrastructure.Data.MainBoundedContext.Migrations.AutoConfiguration(true);

            var context = _dbContextFactory.CreateDbContext<Infrastructure.Data.MainBoundedContext.UnitOfWork.BoundedContextUnitOfWork>(serviceHeader);

            autoConfiguration.InitializeDatabase(context);

            return true;
        }

        public bool SeedEnumerations()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _enumerationAppService.SeedEnumerations(serviceHeader);
        }

        private List<ApplicationDomainWrapper> GetCachedApplicationDomains()
        {
            return _appCache.GetOrAdd<List<ApplicationDomainWrapper>>("{2491EE34-A0C2-4574-B1D0-6727F0DF7D74}", () =>
            {
                var applicationDomainWrapperList = new List<ApplicationDomainWrapper>();

                var settingsCollection = ConfigurationManager.ConnectionStrings;

                if (settingsCollection != null)
                {
                    var exclusion_buffer = new string[] { "BLOBSTORE" };

                    foreach (ConnectionStringSettings item in settingsCollection)
                    {
                        if (!item.Name.ToUpper().In(exclusion_buffer))
                        {
                            applicationDomainWrapperList.Add(new ApplicationDomainWrapper
                            {
                                DomainName = item.Name,
                                ProviderName = item.ProviderName,
                                CreatedDate = DateTime.Now
                            });
                        }
                    }
                }

                return applicationDomainWrapperList;
            });
        }
    }

    public static class ApplicationDomainWrapperSpecifications
    {
        public static Specification<ApplicationDomainWrapper> DefaultSpec(string text)
        {
            Specification<ApplicationDomainWrapper> specification = new TrueSpecification<ApplicationDomainWrapper>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var domainNameSpec = new DirectSpecification<ApplicationDomainWrapper>(c => c.DomainName.Contains(text, StringComparison.OrdinalIgnoreCase));

                var providerNameSpec = new DirectSpecification<ApplicationDomainWrapper>(c => c.ProviderName.Contains(text, StringComparison.OrdinalIgnoreCase));

                specification &= (domainNameSpec | providerNameSpec);
            }

            return specification;
        }
    }
}
