﻿using Application.MainBoundedContext;
using Application.MainBoundedContext.MessagingModule;
using Application.MainBoundedContext.Services;
using Domain.Seedwork;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Data.MainBoundedContext.Repositories;
using Infrastructure.Data.MainBoundedContext.UnitOfWork;
using LazyCache;
using Microsoft.Practices.Unity;
using Numero3.EntityFramework.Implementation;
using Numero3.EntityFramework.Interfaces;
using System.Runtime.Caching;

namespace DistributedServices.MainBoundedContext.UnityContainers
{
    /// <summary>
    /// DI container accessor
    /// </summary>
    public static class Container
    {
        #region Properties

        /// <summary>
        /// Get the current configured container
        /// </summary>
        /// <returns>Configured container</returns>
        public static IUnityContainer Current { get; private set; }

        #endregion

        #region Constructor

        static Container()
        {
            ConfigureContainer();
            ConfigureFactories();
        }

        #endregion

        #region Methods

        private static void ConfigureContainer()
        {
            /*
             * Add here the code configuration or the call to configure the container 
             * using the application configuration file
             */

            Current = new UnityContainer();

            //-> Caching
            Current.RegisterType<IAppCache, CachingService>(new PerResolveLifetimeManager(), new InjectionConstructor(MemoryCache.Default));

            //-> DbContext
            Current.RegisterType<IDbContextFactory, RuntimeContextFactory>(new PerResolveLifetimeManager());
            Current.RegisterType<IDbContextScopeFactory, DbContextScopeFactory>(new PerResolveLifetimeManager());
            Current.RegisterType<IAmbientDbContextLocator, AmbientDbContextLocator>(new PerResolveLifetimeManager());

            //-> Adapters
            Current.RegisterType<ITypeAdapterFactory, AutomapperTypeAdapterFactory>(new ContainerControlledLifetimeManager());

            //-> Repositories	
            Current.RegisterType(typeof(IRepository<>), typeof(Repository<>));

            //-> Domain services

            //-> Application services
            Current.RegisterType<IAuditLogAppService, AuditLogAppService>();
            Current.RegisterType<ISqlCommandAppService, SqlCommandAppService>();
            Current.RegisterType<IEnumerationAppService, EnumerationAppService>();
            Current.RegisterType<IMediaAppService, MediaAppService>();
           
            Current.RegisterType<IPostAppService, PostAppService>();
            

            //-> Distributed Services
        }

        private static void ConfigureFactories()
        {
            LoggerFactory.SetCurrent(new EntLibLogFactory());

            var typeAdapterFactory = Current.Resolve<ITypeAdapterFactory>();
            TypeAdapterFactory.SetCurrent(typeAdapterFactory);
        }

        #endregion
    }
}