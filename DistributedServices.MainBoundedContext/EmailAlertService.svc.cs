﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Application.MainBoundedContext.MessagingModule;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using DistributedServices.Seedwork.ErrorHandlers;
using Microsoft.Practices.Unity.Utility;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandlerAttribute()]
    [UnityInstanceProviderServiceBehavior()]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class EmailAlertService : IEmailAlertService
    {
        private readonly IEmailAlertAppService _emailAlertAppService;

        public EmailAlertService(
            IEmailAlertAppService emailAlertAppService)
        {
            Guard.ArgumentNotNull(emailAlertAppService, "emailAlertAppService");

            _emailAlertAppService = emailAlertAppService;
        }

        #region Email Alert

        public EmailAlertDTO AddEmailAlert(EmailAlertDTO emailAlertDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.AddNewEmailAlert(emailAlertDTO, serviceHeader);
        }

        public bool AddEmailAlerts(List<EmailAlertDTO> emailAlertDTOs)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.AddNewEmailAlerts(emailAlertDTOs, serviceHeader);
        }

        public bool UpdateEmailAlert(EmailAlertDTO emailAlertDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.UpdateEmailAlert(emailAlertDTO, serviceHeader);
        }

        public List<EmailAlertDTO> FindEmailAlerts()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.FindEmailAlerts(serviceHeader);
        }

        public List<EmailAlertDTO> FindEmailAlertsByDLRStatus(int dlrStatus)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.FindEmailAlertsByDLRStatus(dlrStatus, serviceHeader);
        }

        public PageCollectionInfo<EmailAlertDTO> FindEmailAlertsInPage(int pageIndex, int pageSize, List<string> sortFields, bool ascending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.FindEmailAlerts(pageIndex, pageSize, sortFields, ascending, serviceHeader);
        }

        public PageCollectionInfo<EmailAlertDTO> FindEmailAlertsByFilterInPage(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.FindEmailAlerts(dlrStatus, text, pageIndex, pageSize, sortFields, ascending, serviceHeader);
        }

        public PageCollectionInfo<EmailAlertDTO> FindEmailAlertsByDateRangeAndFilterInPage(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.FindEmailAlerts(dlrStatus, startDate, endDate, text, pageIndex, pageSize, sortFields, ascending, serviceHeader);
        }

        public List<EmailAlertDTO> FindEmailAlertsByDLRStatusAndOrigin(int dlrStatus, int origin)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.FindEmailAlertsByDLRStatusAndOrigin(dlrStatus, origin, serviceHeader);
        }

        public EmailAlertDTO FindEmailAlert(Guid emailAlertId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.FindEmailAlert(emailAlertId, serviceHeader);
        }

        #endregion
    }
}
