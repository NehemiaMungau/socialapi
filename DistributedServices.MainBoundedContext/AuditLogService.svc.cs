﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.Services;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using DistributedServices.Seedwork.ErrorHandlers;
using Microsoft.Practices.Unity.Utility;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandlerAttribute()]
    [UnityInstanceProviderServiceBehavior()]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AuditLogService : IAuditLogService
    {
        private readonly IAuditLogAppService _auditLogAppService;

        public AuditLogService(IAuditLogAppService auditLogAppService)
        {
            Guard.ArgumentNotNull(auditLogAppService, "auditLogAppService");

            _auditLogAppService = auditLogAppService;
        }

        #region Audit Log

        public AuditLogDTO AddAuditLog(AuditLogDTO auditLogDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _auditLogAppService.AddNewAuditLog(auditLogDTO, serviceHeader);
        }

        public AuditLogDTO FindAuditLog(Guid auditLogId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _auditLogAppService.FindAuditLog(auditLogId, serviceHeader);
        }

        public List<AuditLogDTO> FindAuditLogs()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _auditLogAppService.FindAuditLogs(serviceHeader);
        }

        public PageCollectionInfo<AuditLogDTO> FindAuditLogsByDateRangeAndFilterInPage(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string filter)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _auditLogAppService.FindAuditLogsByDateRangeAndFilter(pageIndex, pageSize, startDate, endDate, filter, serviceHeader);
        }

        public List<AuditLogDTO> FindAuditLogsByFilter(string filter)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _auditLogAppService.FindAuditLogs(filter, serviceHeader);
        }

        public PageCollectionInfo<AuditLogDTO> FindAuditLogsInPage(int pageIndex, int pageSize)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _auditLogAppService.FindAuditLogs(pageIndex, pageSize, serviceHeader);
        }

        #endregion
    }
}
