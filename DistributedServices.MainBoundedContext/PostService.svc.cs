﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Application.MainBoundedContext;
using Application.MainBoundedContext.DTO;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using DistributedServices.Seedwork.ErrorHandlers;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandlerAttribute()]
    [UnityInstanceProviderServiceBehavior()]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class PostService : IPostService
    {
        private readonly IPostAppService _postAppService;

        public PostService(IPostAppService postAppService)
        {
            Guard.ArgumentNotNull(postAppService, "postAppService");

            _postAppService = postAppService;
        }

        public CommentDTO AddNewComment(CommentDTO postDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _postAppService.AddNewComment(postDTO, serviceHeader);
        }

        public LikeDTO AddNewLikeDTO(LikeDTO likeDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

           return  _postAppService.AddNewLikeDTO(likeDTO, serviceHeader);
        }

        public PostDTO AddNewPost(PostDTO postDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _postAppService.AddNewPost(postDTO, serviceHeader);
        }

        public CommentDTO FindCommentById(Guid commentId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _postAppService.FindCommentById(commentId,serviceHeader);
        }

        public List<CommentDTO> FindComments(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _postAppService.FindComments(id, serviceHeader);
        }

        public LikeDTO FindLikeDTOById(Guid likeId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _postAppService.FindLikeDTOById(likeId, serviceHeader);
        }

        public List<LikeDTO> FindLikes(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _postAppService.FindLikes(id, serviceHeader);
        }

        public PostDTO FindPostById(Guid postId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _postAppService.FindPostById(postId, serviceHeader);
        }

        public List<PostDTO> FindPosts()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _postAppService.FindPosts(serviceHeader);
        }
    }
}
