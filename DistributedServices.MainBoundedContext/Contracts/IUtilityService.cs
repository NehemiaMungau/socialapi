﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.ErrorHandlers;
using System.ServiceModel;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IUtilityService
    {
        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        PageCollectionInfo<ApplicationDomainWrapper> FindApplicationDomainsByFilterInPage(string text, int pageIndex, int pageSize);

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        bool ConfigureApplicationDatabase();
        
        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        bool SeedEnumerations();
    }
}
