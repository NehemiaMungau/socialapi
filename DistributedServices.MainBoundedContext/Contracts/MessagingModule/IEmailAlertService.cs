﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IEmailAlertService
    {
        #region Email Alert

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        EmailAlertDTO AddEmailAlert(EmailAlertDTO emailAlertDTO);

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        bool AddEmailAlerts(List<EmailAlertDTO> emailAlertDTOs);

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        bool UpdateEmailAlert(EmailAlertDTO emailAlertDTO);

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        List<EmailAlertDTO> FindEmailAlerts();

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        List<EmailAlertDTO> FindEmailAlertsByDLRStatus(int dlrStatus);

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        PageCollectionInfo<EmailAlertDTO> FindEmailAlertsInPage(int pageIndex, int pageSize, List<string> sortFields, bool ascending);

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        PageCollectionInfo<EmailAlertDTO> FindEmailAlertsByFilterInPage(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending);

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        PageCollectionInfo<EmailAlertDTO> FindEmailAlertsByDateRangeAndFilterInPage(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending);

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        List<EmailAlertDTO> FindEmailAlertsByDLRStatusAndOrigin(int dlrStatus, int origin);

        [OperationContract()]
        [FaultContract(typeof(ApplicationServiceError))]
        EmailAlertDTO FindEmailAlert(Guid emailAlertId);
        
        #endregion
    }
}
