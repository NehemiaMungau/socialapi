﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DistributedServices.MainBoundedContext
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPostService" in both code and config file together.
    [ServiceContract]
    public interface IPostService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        PostDTO AddNewPost(PostDTO postDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        CommentDTO AddNewComment(CommentDTO postDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        LikeDTO AddNewLikeDTO(LikeDTO likeDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        PostDTO FindPostById(Guid postId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        CommentDTO FindCommentById(Guid commentId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        LikeDTO FindLikeDTOById(Guid likeId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        List<PostDTO> FindPosts();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        List<CommentDTO> FindComments(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        List<LikeDTO> FindLikes(Guid id);
    }
}
