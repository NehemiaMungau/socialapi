﻿using System;

namespace DistributedServices.Seedwork.EndpointBehaviors
{
    public static class CustomHeaderNames
    {
        public const String CustomHeaderName = "CustomHeader";

        public const String KeyName = "Key";

        public const String CustomHeaderNamespace = "http://schemas.devleap.com/CustomHeader";
    }
}
