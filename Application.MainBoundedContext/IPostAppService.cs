﻿using Application.MainBoundedContext.DTO;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.MainBoundedContext
{
    public interface IPostAppService
    {
        PostDTO AddNewPost(PostDTO postDTO, ServiceHeader serviceHeader);

        CommentDTO AddNewComment(CommentDTO postDTO, ServiceHeader serviceHeader);

        LikeDTO AddNewLikeDTO(LikeDTO likeDTO, ServiceHeader serviceHeader);
        
        PostDTO FindPostById(Guid postId, ServiceHeader serviceHeader);

        CommentDTO FindCommentById(Guid commentId, ServiceHeader serviceHeader);

        LikeDTO FindLikeDTOById(Guid likeId, ServiceHeader serviceHeader);

        List<PostDTO> FindPosts(ServiceHeader serviceHeader);

        List<CommentDTO> FindComments(Guid id,ServiceHeader serviceHeader);

        List<LikeDTO> FindLikes(Guid id,ServiceHeader serviceHeader);

    }
}
