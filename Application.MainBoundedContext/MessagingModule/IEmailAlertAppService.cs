﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;

namespace Application.MainBoundedContext.MessagingModule
{
    public interface IEmailAlertAppService
    {
        EmailAlertDTO AddNewEmailAlert(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader);

        bool AddNewEmailAlerts(List<EmailAlertDTO> emailAlertDTOs, ServiceHeader serviceHeader);

        bool UpdateEmailAlert(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader);

        List<EmailAlertDTO> FindEmailAlerts(ServiceHeader serviceHeader);

        List<EmailAlertDTO> FindEmailAlertsByDLRStatus(int dlrStatus, ServiceHeader serviceHeader);

        PageCollectionInfo<EmailAlertDTO> FindEmailAlerts(int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader);

        PageCollectionInfo<EmailAlertDTO> FindEmailAlerts(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader);

        PageCollectionInfo<EmailAlertDTO> FindEmailAlerts(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader);

        List<EmailAlertDTO> FindEmailAlertsByDLRStatusAndOrigin(int dlrStatus, int origin, ServiceHeader serviceHeader);

        EmailAlertDTO FindEmailAlert(Guid emailAlertId, ServiceHeader serviceHeader);
    }
}
