﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Application.Seedwork;
using Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg;
using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.MainBoundedContext.MessagingModule
{
    public class EmailAlertAppService : IEmailAlertAppService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IRepository<EmailAlert> _emailAlertRepository;

        public EmailAlertAppService(
            IDbContextScopeFactory dbContextScopeFactory,
            IRepository<EmailAlert> emailAlertRepository)
        {
            if (dbContextScopeFactory == null)
                throw new ArgumentNullException("dbContextScopeFactory");

            if (emailAlertRepository == null)
                throw new ArgumentNullException("emailAlertRepository");

            _dbContextScopeFactory = dbContextScopeFactory;
            _emailAlertRepository = emailAlertRepository;
        }

        public EmailAlertDTO AddNewEmailAlert(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader)
        {
            if (emailAlertDTO != null)
            {
                emailAlertDTO.ValidateAll();

                if (emailAlertDTO.HasErrors)
                    throw new InvalidOperationException(string.Join(Environment.NewLine, emailAlertDTO.ErrorMessages));

                var dlrStatus = DLRStatus.UnKnown;

                switch ((MessageOrigin)emailAlertDTO.MailMessageOrigin)
                {
                    case MessageOrigin.Within:
                    case MessageOrigin.Without:
                        dlrStatus = DLRStatus.Pending;
                        break;
                    case MessageOrigin.Other:
                        dlrStatus = DLRStatus.NotApplicable;
                        break;
                    default:
                        break;
                }

                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    var mailMessage = new MailMessage(emailAlertDTO.MailMessageFrom, emailAlertDTO.MailMessageTo, emailAlertDTO.MailMessageCC, emailAlertDTO.MailMessageSubject, emailAlertDTO.MailMessageBody, emailAlertDTO.MailMessageIsBodyHtml, (int)dlrStatus, emailAlertDTO.MailMessageOrigin, emailAlertDTO.MailMessagePriority, 0, emailAlertDTO.MailMessageSecurityCritical);

                    var emailAlert = EmailAlertFactory.CreateEmailAlert(mailMessage);

                    _emailAlertRepository.Add(emailAlert, serviceHeader);

                    dbContextScope.SaveChanges(serviceHeader);

                    return emailAlert.ProjectedAs<EmailAlertDTO>();
                }
            }
            else return null;
        }

        public bool AddNewEmailAlerts(List<EmailAlertDTO> emailAlertDTOs, ServiceHeader serviceHeader)
        {
            var result = default(bool);

            if (emailAlertDTOs != null && emailAlertDTOs.Any())
            {
                emailAlertDTOs.ForEach(item =>
                {
                    AddNewEmailAlert(item, serviceHeader);
                });

                result = true;
            }

            return result;
        }

        public bool UpdateEmailAlert(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader)
        {
            if (emailAlertDTO == null || emailAlertDTO.Id == Guid.Empty)
                return false;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = _emailAlertRepository.Get(emailAlertDTO.Id, serviceHeader);

                if (persisted != null)
                {
                    var mailMessage = new MailMessage(persisted.MailMessage.From, persisted.MailMessage.To, persisted.MailMessage.CC, persisted.MailMessage.Subject, persisted.MailMessage.Body, persisted.MailMessage.IsBodyHtml, emailAlertDTO.MailMessageDLRStatus, persisted.MailMessage.Origin, persisted.MailMessage.Priority, emailAlertDTO.MailMessageSendRetry, persisted.MailMessage.SecurityCritical);

                    var current = EmailAlertFactory.CreateEmailAlert(mailMessage);

                    current.CreatedDate = persisted.CreatedDate;
                    current.ChangeCurrentIdentity(persisted.Id);

                    _emailAlertRepository.Merge(persisted, current, serviceHeader);

                    return dbContextScope.SaveChanges(serviceHeader) >= 0;
                }
                else return false;
            }
        }

        public List<EmailAlertDTO> FindEmailAlerts(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var emailAlerts = _emailAlertRepository.GetAll(serviceHeader);

                if (emailAlerts != null && emailAlerts.Any())
                {
                    return emailAlerts.ProjectedAsCollection<EmailAlertDTO>();
                }
                else return null;
            }
        }

        public List<EmailAlertDTO> FindEmailAlertsByDLRStatus(int dlrStatus, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = EmailAlertSpecifications.EmailAlertWithDLRStatus(dlrStatus);

                ISpecification<EmailAlert> spec = filter;

                var emailAlerts = _emailAlertRepository.AllMatching(spec, serviceHeader);

                if (emailAlerts != null && emailAlerts.Any())
                {
                    return emailAlerts.ProjectedAsCollection<EmailAlertDTO>();
                }
                else return null;
            }
        }

        public EmailAlertDTO FindEmailAlert(Guid emailAlertId, ServiceHeader serviceHeader)
        {
            if (emailAlertId != Guid.Empty)
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var emailAlert = _emailAlertRepository.Get(emailAlertId, serviceHeader);

                    if (emailAlert != null)
                    {
                        return emailAlert.ProjectedAs<EmailAlertDTO>();
                    }
                    else return null;
                }
            }
            else return null;
        }

        public PageCollectionInfo<EmailAlertDTO> FindEmailAlerts(int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = EmailAlertSpecifications.DefaultSpec();

                ISpecification<EmailAlert> spec = filter;

                var emailAlertPagedCollection = _emailAlertRepository.AllMatchingPaged(spec, pageIndex, pageSize, sortFields, ascending, serviceHeader);

                if (emailAlertPagedCollection != null)
                {
                    var pageCollection = emailAlertPagedCollection.Items.ProjectedAsCollection<EmailAlertDTO>();

                    var itemsCount = emailAlertPagedCollection.TotalItems;

                    return new PageCollectionInfo<EmailAlertDTO> { PageCollection = pageCollection, ItemsCount = itemsCount };
                }
                else return null;
            }
        }

        public PageCollectionInfo<EmailAlertDTO> FindEmailAlerts(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = EmailAlertSpecifications.EmailAlertFullText(dlrStatus, text);

                ISpecification<EmailAlert> spec = filter;

                var emailAlertPagedCollection = _emailAlertRepository.AllMatchingPaged(spec, pageIndex, pageSize, sortFields, ascending, serviceHeader);

                if (emailAlertPagedCollection != null)
                {
                    var pageCollection = emailAlertPagedCollection.Items.ProjectedAsCollection<EmailAlertDTO>();

                    var itemsCount = emailAlertPagedCollection.TotalItems;

                    return new PageCollectionInfo<EmailAlertDTO> { PageCollection = pageCollection, ItemsCount = itemsCount };
                }
                else return null;
            }
        }

        public PageCollectionInfo<EmailAlertDTO> FindEmailAlerts(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = EmailAlertSpecifications.EmailAlertFullText(dlrStatus, startDate, endDate, text);

                ISpecification<EmailAlert> spec = filter;

                var emailAlertPagedCollection = _emailAlertRepository.AllMatchingPaged(spec, pageIndex, pageSize, sortFields, ascending, serviceHeader);

                if (emailAlertPagedCollection != null)
                {
                    var pageCollection = emailAlertPagedCollection.Items.ProjectedAsCollection<EmailAlertDTO>();

                    var itemsCount = emailAlertPagedCollection.TotalItems;

                    return new PageCollectionInfo<EmailAlertDTO> { PageCollection = pageCollection, ItemsCount = itemsCount };
                }
                else return null;
            }
        }

        public List<EmailAlertDTO> FindEmailAlertsByDLRStatusAndOrigin(int dlrStatus, int origin, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = EmailAlertSpecifications.EmailAlertWithDLRStatusAndOrigin(dlrStatus, origin);

                ISpecification<EmailAlert> spec = filter;

                var emailAlerts = _emailAlertRepository.AllMatching(spec, serviceHeader);

                if (emailAlerts != null && emailAlerts.Any())
                {
                    return emailAlerts.ProjectedAsCollection<EmailAlertDTO>();
                }
                else return null;
            }
        }
    }
}
