﻿using Infrastructure.Crosscutting.Framework.Utils;
using System.Collections.Generic;

namespace Application.MainBoundedContext.Services
{
    public interface ISqlCommandAppService
    {
        bool BulkInsert<T>(string tableName, IList<T> list, ServiceHeader serviceHeader);
    }
}
