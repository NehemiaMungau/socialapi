﻿using Application.MainBoundedContext.DTO;
using Application.Seedwork;
using Domain.MainBoundedContext.Aggregates.AuditLogAgg;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.MainBoundedContext.Services
{
    public class AuditLogAppService : IAuditLogAppService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IRepository<AuditLog> _auditLogRepository;

        public AuditLogAppService(
           IDbContextScopeFactory dbContextScopeFactory,
           IRepository<AuditLog> auditLogRepository)
        {
            if (dbContextScopeFactory == null)
                throw new ArgumentNullException("dbContextScopeFactory");

            if (auditLogRepository == null)
                throw new ArgumentNullException("auditLogRepository");

            _dbContextScopeFactory = dbContextScopeFactory;
            _auditLogRepository = auditLogRepository;
        }

        public AuditLogDTO AddNewAuditLog(AuditLogDTO auditLogDTO, ServiceHeader serviceHeader)
        {
            if (auditLogDTO != null)
            {
                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    var auditLog = AuditLogFactory.CreateAuditLog(auditLogDTO.EventType, auditLogDTO.TableName, auditLogDTO.RecordID, auditLogDTO.AdditionalNarration, serviceHeader);

                    _auditLogRepository.Add(auditLog, serviceHeader);

                    dbContextScope.SaveChanges(serviceHeader);

                    return auditLog.ProjectedAs<AuditLogDTO>();
                }
            }
            else return null;
        }

        public AuditLogDTO FindAuditLog(Guid auditLogId, ServiceHeader serviceHeader)
        {
            if (auditLogId != Guid.Empty)
            {
                using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    var auditLog = _auditLogRepository.Get(auditLogId, serviceHeader);

                    if (auditLog != null)
                    {
                        return auditLog.ProjectedAs<AuditLogDTO>();
                    }
                    else return null;
                }
            }
            else return null;
        }

        public List<AuditLogDTO> FindAuditLogs(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                var auditLogs = _auditLogRepository.GetAll(serviceHeader);

                if (auditLogs != null && auditLogs.Any())
                {
                    return auditLogs.ProjectedAsCollection<AuditLogDTO>();
                }
                else return null;
            }
        }

        public PageCollectionInfo<AuditLogDTO> FindAuditLogs(int pageIndex, int pageSize, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                var filter = AuditLogSpecifications.DefaultSpec();

                ISpecification<AuditLog> spec = filter;

                var sortFields = new List<string> { "Id" };

                var auditLogPagedCollection = _auditLogRepository.AllMatchingPaged(spec, pageIndex, pageSize, sortFields, true, serviceHeader);

                if (auditLogPagedCollection != null)
                {
                    var pageCollection = auditLogPagedCollection.Items.ProjectedAsCollection<AuditLogDTO>();

                    var itemsCount = auditLogPagedCollection.TotalItems;

                    return new PageCollectionInfo<AuditLogDTO> { PageCollection = pageCollection, ItemsCount = itemsCount };
                }
                else return null;
            }
        }

        public PageCollectionInfo<AuditLogDTO> FindAuditLogsByDateRangeAndFilter(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string text, ServiceHeader serviceHeader)
        {
            if (startDate != null && endDate != null)
            {
                using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    var filter = AuditLogSpecifications.AuditLogWithDateRangeAndFullText(startDate, endDate, text);

                    ISpecification<AuditLog> spec = filter;

                    var sortFields = new List<string> { "Id" };

                    var auditLogPagedCollection = _auditLogRepository.AllMatchingPaged(spec, pageIndex, pageSize, sortFields, true, serviceHeader);

                    if (auditLogPagedCollection != null)
                    {
                        var pageCollection = auditLogPagedCollection.Items.ProjectedAsCollection<AuditLogDTO>();

                        var itemsCount = auditLogPagedCollection.TotalItems;

                        return new PageCollectionInfo<AuditLogDTO> { PageCollection = pageCollection, ItemsCount = itemsCount };
                    }
                    else return null;
                }
            }
            else
                return null;
        }

        public List<AuditLogDTO> FindAuditLogs(string text, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                var filter = AuditLogSpecifications.AuditLogFullText(text);

                ISpecification<AuditLog> spec = filter;

                var auditLogs = _auditLogRepository.AllMatching(spec, serviceHeader);

                if (auditLogs != null && auditLogs.Any())
                {
                    return auditLogs.ProjectedAsCollection<AuditLogDTO>();
                }
                else return null;
            }
        }
    }
}
