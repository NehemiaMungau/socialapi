﻿using Application.MainBoundedContext.DTO;
using Infrastructure.Crosscutting.Framework.Utils;
using System;

namespace Application.MainBoundedContext.Services
{
    public interface IMediaAppService
    {
        MediaDTO GetMedia(Guid sku, string blobDatabaseConnectionString);

        bool PostFile(MediaDTO mediaDTO, string fileUploadDirectory, string blobDatabaseConnectionString, ServiceHeader serviceHeader);

        bool PostImage(MediaDTO mediaDTO, string fileUploadDirectory, string blobDatabaseConnectionString, ServiceHeader serviceHeader);
    }
}
