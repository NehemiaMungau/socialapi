﻿using Application.MainBoundedContext.DTO;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing.Imaging;
using System.IO;

namespace Application.MainBoundedContext.Services
{
    public class MediaAppService : IMediaAppService
    {
        public MediaDTO GetMedia(Guid sku, string blobDatabaseConnectionString)
        {
            FileDownloadModel file = null;

            if (GetFile(sku, blobDatabaseConnectionString, out file))
            {
                MediaDTO mediaDTO = new MediaDTO
                {
                    SKU = sku,
                    FileName = file.FileName,
                    ContentCoding = file.ContentCoding,
                    ContentLength = file.ContentLength,
                    ContentType = file.ContentType,
                };

                using (var reader = new BinaryReader(file.Content))
                {
                    if (mediaDTO.ContentType.Equals("image/tiff", StringComparison.OrdinalIgnoreCase))
                    {
                        mediaDTO.Content = TIFFToJPEG(reader.ReadBytes((int)file.ContentLength));
                    }
                    else if (mediaDTO.ContentType.Equals("image/gif", StringComparison.OrdinalIgnoreCase))
                    {
                        mediaDTO.Content = GIFToJPEG(reader.ReadBytes((int)file.ContentLength));
                    }
                    else mediaDTO.Content = reader.ReadBytes((int)file.ContentLength);
                }

                return mediaDTO;
            }
            else return null;
        }

        public bool PostFile(MediaDTO mediaDTO, string fileUploadDirectory, string blobDatabaseConnectionString, ServiceHeader serviceHeader)
        {
            if (mediaDTO != null && mediaDTO.SKU != Guid.Empty)
            {
                var path = Path.Combine(fileUploadDirectory, mediaDTO.FileName);

                if (System.IO.File.Exists(path))
                {
                    PostFile(mediaDTO.SKU, path, string.Format("<{0}>-{1}", mediaDTO.FileType, mediaDTO.FileRemarks), blobDatabaseConnectionString, serviceHeader);

                    return true;
                }
                else return false;
            }
            else return false;
        }

        public bool PostImage(MediaDTO mediaDTO, string fileUploadDirectory, string blobDatabaseConnectionString, ServiceHeader serviceHeader)
        {
            if (mediaDTO != null && mediaDTO.SKU != Guid.Empty)
            {
                var image = new Domain.MainBoundedContext.ValueObjects.Image(mediaDTO.Content ?? new byte[0] { });

                if (image.Buffer.Length != 0)
                {
                    var fileExtension = ".jpg";

                    mediaDTO.ContentType = mediaDTO.ContentType ?? "image/jpeg";

                    if (mediaDTO.ContentType.Equals("image/tiff", StringComparison.OrdinalIgnoreCase))
                        fileExtension = ".tif";

                    var fileName = Path.Combine(fileUploadDirectory, string.Format("{0}{1}", mediaDTO.SKU.ToString("D"), fileExtension));

                    using (MemoryStream stream = new MemoryStream(image.Buffer))
                    {
                        using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(stream))
                        {
                            ImageFormat format = ImageFormat.Jpeg;

                            if (mediaDTO.ContentType.Equals("image/tiff", StringComparison.OrdinalIgnoreCase))
                                format = ImageFormat.Tiff;

                            bitmap.Save(fileName, format);
                        }
                    }

                    PostFile(mediaDTO.SKU, fileName, string.Format("<{0}>-{1}", mediaDTO.FileType, mediaDTO.FileRemarks), blobDatabaseConnectionString, serviceHeader);

                    return true;
                }
                else return false;
            }
            else return false;
        }

        private byte[] TIFFToJPEG(byte[] tiffBytes)
        {
            try
            {
                byte[] jpegBytes;

                using (MemoryStream inStream = new MemoryStream(tiffBytes))
                using (MemoryStream outStream = new MemoryStream())
                {
                    System.Drawing.Bitmap.FromStream(inStream).Save(outStream, ImageFormat.Jpeg);

                    jpegBytes = outStream.ToArray();
                }

                return jpegBytes;
            }
            catch
            {
                return null;
            }
        }

        private byte[] GIFToJPEG(byte[] gifBytes)
        {
            try
            {
                byte[] jpegBytes;

                using (MemoryStream inStream = new MemoryStream(gifBytes))
                using (MemoryStream outStream = new MemoryStream())
                {
                    System.Drawing.Bitmap.FromStream(inStream).Save(outStream, ImageFormat.Jpeg);

                    jpegBytes = outStream.ToArray();
                }

                return jpegBytes;
            }
            catch
            {
                return null;
            }
        }

        private SqlConnection GetConnection(string connectionString)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
            builder.Pooling = true;
            builder.AsynchronousProcessing = true;

            SqlConnection conn = new SqlConnection(builder.ConnectionString);
            conn.Open();

            return conn;
        }

        private bool GetFile(Guid sku, string connectionString, out FileDownloadModel file)
        {
            SqlConnection conn = GetConnection(connectionString);
            SqlTransaction trn = conn.BeginTransaction();

            try
            {
                SqlCommand cmd = new SqlCommand(
                @"SELECT file_name,
            	                content_type,
                                content_coding,
                                DATALENGTH (content) as content_length,
            	            content.PathName() as path,
                                GET_FILESTREAM_TRANSACTION_CONTEXT ()
                            FROM bayes_media
                            WHERE media_sku = @media_sku;", conn, trn);

                SqlParameter paramSku = new SqlParameter("@media_sku", SqlDbType.UniqueIdentifier);
                paramSku.Value = sku;
                cmd.Parameters.Add(paramSku);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (false == reader.Read())
                    {
                        reader.Close();
                        trn.Dispose();
                        conn.Dispose();
                        trn = null;
                        conn = null;
                        file = null;
                        return false;
                    }

                    string contentDisposition = reader.GetString(0);
                    string contentType = reader.GetString(1);
                    string contentCoding = reader.IsDBNull(2) ? null : reader.GetString(2);
                    long contentLength = reader.GetInt64(3);
                    string path = reader.GetString(4);
                    byte[] context = reader.GetSqlBytes(5).Buffer;

                    file = new FileDownloadModel
                    {
                        FileName = contentDisposition,
                        ContentCoding = contentCoding,
                        ContentType = contentType,
                        ContentLength = contentLength,
                        Content = new MvcResultSqlFileStream
                        {
                            SqlStream = new SqlFileStream(path, context, FileAccess.Read),
                            Connection = conn,
                            Transaction = trn
                        }
                    };
                    conn = null; // ownership transfered to the stream
                    trn = null;
                    return true;
                }
            }
            finally
            {
                if (null != trn)
                {
                    trn.Dispose();
                }
                if (null != conn)
                {
                    conn.Dispose();
                }
            }
        }

        private void PostFile(Guid sku, string fileName, string fileRemarks, string connectionString, ServiceHeader serviceHeader)
        {
            using (SqlConnection conn = GetConnection(connectionString))
            {
                using (SqlCommand cmdDelete = new SqlCommand(@"DELETE FROM bayes_media WHERE media_sku = @media_sku;", conn))
                {
                    SqlParameter paramSku = new SqlParameter("@media_sku", SqlDbType.UniqueIdentifier);
                    paramSku.Value = sku;
                    cmdDelete.Parameters.Add(paramSku);

                    cmdDelete.ExecuteNonQuery();
                }

                using (SqlTransaction trn = conn.BeginTransaction())
                {
                    SqlCommand cmdInsert = new SqlCommand(
                    @"insert into bayes_media 
                        (media_sku, file_name, file_remarks, content_type, content_coding, content, created_by)
                    output 
	                    INSERTED.content.PathName(),    
                        GET_FILESTREAM_TRANSACTION_CONTEXT ()
                    values 
                        (@media_sku, @content_disposition, @file_remarks, @content_type, @content_coding, 0x, @created_by)", conn, trn);

                    cmdInsert.Parameters.Add("@media_sku", SqlDbType.UniqueIdentifier);
                    cmdInsert.Parameters["@media_sku"].Value = sku;

                    cmdInsert.Parameters.Add("@content_disposition", SqlDbType.VarChar, 256);
                    cmdInsert.Parameters["@content_disposition"].Value = fileName;

                    cmdInsert.Parameters.Add("@file_remarks", SqlDbType.VarChar, 256);
                    cmdInsert.Parameters["@file_remarks"].Value = fileRemarks;

                    cmdInsert.Parameters.Add("@content_type", SqlDbType.VarChar, 256);
                    cmdInsert.Parameters["@content_type"].Value = System.Web.MimeMapping.GetMimeMapping(fileName);

                    cmdInsert.Parameters.Add("@content_coding", SqlDbType.VarChar, 256);
                    cmdInsert.Parameters["@content_coding"].Value = DBNull.Value;

                    cmdInsert.Parameters.Add("@created_by", SqlDbType.VarChar, 256);
                    cmdInsert.Parameters["@created_by"].Value = serviceHeader.ApplicationUserName;

                    string serverPath = null;
                    byte[] serverTxn = null;

                    // cmdInsert is an INSERT command that uses the OUTPUT clause
                    // Thus we use the ExecuteReader to get the 
                    // result set from the output columns
                    //
                    using (SqlDataReader rdr = cmdInsert.ExecuteReader())
                    {
                        rdr.Read();
                        serverPath = rdr.GetString(0);
                        serverTxn = rdr.GetSqlBytes(1).Buffer;
                    }

                    SaveFile(fileName, serverPath, serverTxn);

                    trn.Commit();
                }
            }
        }

        private void SaveFile(string clientPath, string serverPath, byte[] serverTxn)
        {
            const int BlockSize = 1024 * 512;

            using (FileStream source = new FileStream(clientPath, FileMode.Open, FileAccess.Read))
            {
                using (SqlFileStream dest = new SqlFileStream(serverPath, serverTxn, FileAccess.Write))
                {
                    byte[] buffer = new byte[BlockSize];
                    int bytesRead;
                    while ((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        dest.Write(buffer, 0, bytesRead);
                        dest.Flush();
                    }
                    dest.Close();
                }
                source.Close();
            }
        }
    }
}
