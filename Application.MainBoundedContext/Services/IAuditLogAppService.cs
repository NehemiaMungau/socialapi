﻿using Application.MainBoundedContext.DTO;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;

namespace Application.MainBoundedContext.Services
{
    public interface IAuditLogAppService
    {
        AuditLogDTO AddNewAuditLog(AuditLogDTO auditLogDTO, ServiceHeader serviceHeader);

        AuditLogDTO FindAuditLog(Guid auditLogId, ServiceHeader serviceHeader);

        List<AuditLogDTO> FindAuditLogs(ServiceHeader serviceHeader);

        PageCollectionInfo<AuditLogDTO> FindAuditLogs(int pageIndex, int pageSize, ServiceHeader serviceHeader);

        PageCollectionInfo<AuditLogDTO> FindAuditLogsByDateRangeAndFilter(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string text, ServiceHeader serviceHeader);

        List<AuditLogDTO> FindAuditLogs(string text, ServiceHeader serviceHeader);
    }
}
