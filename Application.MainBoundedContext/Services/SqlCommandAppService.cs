﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Application.MainBoundedContext.Services
{
    public class SqlCommandAppService : ISqlCommandAppService
    {
        public bool BulkInsert<T>(string tableName, IList<T> list, ServiceHeader serviceHeader)
        {
            var result = default(bool);

            using (var bulkCopy = new SqlBulkCopy(ConfigurationManager.ConnectionStrings[serviceHeader.ApplicationDomainName].ConnectionString, SqlBulkCopyOptions.FireTriggers))
            {
                bulkCopy.BulkCopyTimeout = 300;
                bulkCopy.BatchSize = 5000;
                bulkCopy.DestinationTableName = tableName;

                var table = new DataTable();

                var props = TypeDescriptor.GetProperties(typeof(T))
                                           //Dirty hack to make sure we only have system data types 
                                           //i.e. filter out the relationships/collections
                                           .Cast<PropertyDescriptor>()
                                           .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
                                           .ToArray();

                foreach (var propertyInfo in props)
                {
                    bulkCopy.ColumnMappings.Add(propertyInfo.Name, propertyInfo.Name);

                    table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
                }

                var values = new object[props.Length];

                foreach (var item in list)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item);
                    }

                    table.Rows.Add(values);
                }

                bulkCopy.WriteToServer(table);

                result = true;
            }

            return result;
        }
    }
}
