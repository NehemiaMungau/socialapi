﻿using Application.MainBoundedContext.DTO;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;

namespace Application.MainBoundedContext.Services
{
    public interface IEnumerationAppService
    {
        bool SeedEnumerations(ServiceHeader serviceHeader);

        EnumerationDTO FindEnumeration(Guid enumerationId, ServiceHeader serviceHeader);

        List<EnumerationDTO> FindEnumerations(ServiceHeader serviceHeader);

        PageCollectionInfo<EnumerationDTO> FindEnumerations(int pageIndex, int pageSize, string text, ServiceHeader serviceHeader);

        List<EnumerationDTO> FindEnumerations(string text, ServiceHeader serviceHeader);
    }
}
