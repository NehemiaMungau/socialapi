﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using Domain.Seedwork;
using Domain.MainBoundedContext.Aggregates.CommentAgg;
using Domain.MainBoundedContext.Aggregates.LikeAgg;
using Domain.MainBoundedContext.Aggregates.PostAgg;
using Application.Seedwork;
using Domain.Seedwork.Specification;

namespace Application.MainBoundedContext
{
    public class PostAppService : IPostAppService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IRepository<Comment> _commentRepository;
        private readonly IRepository<Like> _likeRepository;
        private readonly IRepository<Post> _postRepository;

        public PostAppService(IDbContextScopeFactory dbContextScopeFactory, IRepository<Comment> commentRepository,
            IRepository<Like> likeRepository, IRepository<Post> postRepository
            )
        {
            if (dbContextScopeFactory == null)
                throw new ArgumentNullException(nameof(dbContextScopeFactory));

            if (commentRepository == null)
                throw new ArgumentNullException(nameof(commentRepository));

            if (likeRepository == null)
                throw new ArgumentNullException(nameof(likeRepository));

            if (postRepository == null)
                throw new ArgumentNullException(nameof(postRepository));

            _dbContextScopeFactory = dbContextScopeFactory;

            _commentRepository = commentRepository;

            _likeRepository = likeRepository;

            _postRepository = postRepository;

        }

        public CommentDTO AddNewComment(CommentDTO commentDTO, ServiceHeader serviceHeader)
        {
            if (commentDTO !=null)
            {
                commentDTO.ValidateAll();

                if (commentDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, commentDTO.ErrorMessages));

                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    var comment = CommentFactory.CreateComment(commentDTO.PostId, commentDTO.Content, commentDTO.CreatedBy);

                    _commentRepository.Add(comment, serviceHeader);

                    dbContextScope.SaveChanges(serviceHeader);

                    return comment.ProjectedAs<CommentDTO>();

                }
            }
            return null;
        }

        public LikeDTO AddNewLikeDTO(LikeDTO likeDTO, ServiceHeader serviceHeader)
        {
            if (likeDTO != null)
            {
                likeDTO.ValidateAll();

                if (likeDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, likeDTO.ErrorMessages));

                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    var like = LikeFactory.CreateLike(likeDTO.PostId, likeDTO.CreatedBy);

                    _likeRepository.Add(like, serviceHeader);

                    dbContextScope.SaveChanges(serviceHeader);

                    return like.ProjectedAs<LikeDTO>();

                }
            }
            return null;
        }

        public PostDTO AddNewPost(PostDTO postDTO, ServiceHeader serviceHeader)
        {
            if (postDTO != null)
            {
                postDTO.ValidateAll();

                if (postDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, postDTO.ErrorMessages));

                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    var post = PostFactory.CreatePost(postDTO.CreatedBy);

                    _postRepository.Add(post, serviceHeader);

                    dbContextScope.SaveChanges(serviceHeader);

                    return post.ProjectedAs<PostDTO>();

                }
            }
            return null;
        }

        public CommentDTO FindCommentById(Guid commentId, ServiceHeader serviceHeader)
        {
            if (commentId != Guid.Empty)
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var comment = _commentRepository.Get(commentId, serviceHeader);

                    return comment.ProjectedAs<CommentDTO>();
                }
            }

            return null;
        }

        public List<CommentDTO> FindComments(Guid id,ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                var filter = CommentSpecification.FindCommentsByPostId(id);
                ISpecification<Comment> specification = filter;

                var comments = _commentRepository.AllMatching(specification, serviceHeader);

                if (comments != null && comments.Any())
                {
                    return comments.ProjectedAsCollection<CommentDTO>();

                }
                else return null;
            }
        }

        public LikeDTO FindLikeDTOById(Guid likeId, ServiceHeader serviceHeader)
        {
            if (likeId != Guid.Empty)
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var like = _likeRepository.Get(likeId, serviceHeader);

                    return like.ProjectedAs<LikeDTO>();
                }
            }

            return null;
        }

        public List<LikeDTO> FindLikes(Guid id,ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                var filter = LikeSpecification.FindLikesByPostId(id);
                ISpecification<Like> specification = filter;

                var likes = _likeRepository.AllMatching(specification, serviceHeader);

                if (likes != null && likes.Any())
                {
                    return likes.ProjectedAsCollection<LikeDTO>();

                }
                else return null;
            }
        }

        public PostDTO FindPostById(Guid postId, ServiceHeader serviceHeader)
        {
            if (postId != Guid.Empty)
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var post = _likeRepository.Get(postId, serviceHeader);

                    return post.ProjectedAs<PostDTO>();
                }
            }

            return null;
        }

        public List<PostDTO> FindPosts(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                var filter = PostSpecification.DefaultSpec();
                ISpecification<Post> specification = filter;

                var posts = _postRepository.AllMatching(specification, serviceHeader);

                if (posts != null && posts.Any())
                {
                    return posts.ProjectedAsCollection<PostDTO>();

                }
                else return null;
            }
        }
    }
}
