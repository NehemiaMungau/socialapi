﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.Aggregates.CommentAgg
{
    public static class CommentFactory
    {
        public static Comment CreateComment(Guid postId,string content, string createdBy)
        {
            Comment comment = new Comment();

            comment.GenerateNewIdentity();

            comment.PostId = postId;

            comment.Content = content;

            comment.CreatedBy = createdBy;

            comment.CreatedDate = DateTime.Now;

            return comment;
        }
    }
}
