﻿using Domain.Seedwork.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.Aggregates.CommentAgg
{
    public static  class CommentSpecification
    {
        public static Specification<Comment> DefaultSpec()
        {
            Specification<Comment> specification = new TrueSpecification<Comment>();


            return specification;
        }

        public static Specification<Comment> FindCommentsByPostId(Guid id)
        {
            Specification<Comment> specification = new DirectSpecification<Comment>(c => c.PostId == id); ;


            return specification;
        }

    }
}
