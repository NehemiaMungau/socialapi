﻿using Domain.MainBoundedContext.Aggregates.PostAgg;
using Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.Aggregates.CommentAgg
{
    public class Comment: Entity
    {
        public virtual Post Post { get; set; }
        public Guid PostId { get; set; }

        public string Content { get; set; }


        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
