﻿using Domain.MainBoundedContext.Aggregates.PostAgg;
using System;

namespace Domain.MainBoundedContext.Aggregates.LikeAgg
{
    public class LikeFactory
    {
        public static Like CreateLike(Guid postId, string createdBy)
        {
            Like like = new Like();

            like.GenerateNewIdentity();

            like.PostId = postId;
            
            like.CreatedBy = createdBy;

            like.CreatedDate = DateTime.Now;

            return like;
        }
    }
}
