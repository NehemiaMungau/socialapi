﻿using Domain.Seedwork.Specification;
using System;

namespace Domain.MainBoundedContext.Aggregates.LikeAgg
{
    public static class LikeSpecification
    {
        public static Specification<Like> FindLikesByPostId(Guid id)
        {
            Specification<Like> specification = new DirectSpecification<Like>(c => c.PostId == id); ;


            return specification;
        }
    }
}
