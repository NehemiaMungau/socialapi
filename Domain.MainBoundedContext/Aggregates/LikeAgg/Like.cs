﻿using Domain.MainBoundedContext.Aggregates.PostAgg;
using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.Aggregates.LikeAgg
{
    public class Like: Entity
    {
        public virtual Post Post { get; set; }
        public Guid PostId { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}
