﻿using Domain.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.Aggregates.AuditLogAgg
{
    public static class AuditLogFactory
    {
        public static AuditLog CreateAuditLog(int eventType, string tableName, string recordID, string additionalNarration, ServiceHeader serviceHeader)
        {
            var auditLog = new AuditLog();

            auditLog.GenerateNewIdentity();

            auditLog.EventType = (byte)eventType;

            auditLog.TableName = tableName;

            auditLog.RecordID = recordID;

            auditLog.AdditionalNarration = additionalNarration;

            auditLog.ApplicationUserName = serviceHeader.ApplicationUserName;

            auditLog.EnvironmentUserName = serviceHeader.EnvironmentUserName;

            auditLog.EnvironmentMachineName = serviceHeader.EnvironmentMachineName;

            auditLog.EnvironmentDomainName = serviceHeader.EnvironmentDomainName;

            auditLog.EnvironmentOSVersion = serviceHeader.EnvironmentOSVersion;

            auditLog.EnvironmentMACAddress = serviceHeader.EnvironmentMACAddress;

            auditLog.EnvironmentMotherboardSerialNumber = serviceHeader.EnvironmentMotherboardSerialNumber;

            auditLog.EnvironmentProcessorId = serviceHeader.EnvironmentProcessorId;

            auditLog.EnvironmentIPAddress = serviceHeader.EnvironmentIPAddress;

            auditLog.EventDate = DateTime.Now;

            return auditLog;
        }
    }
}
