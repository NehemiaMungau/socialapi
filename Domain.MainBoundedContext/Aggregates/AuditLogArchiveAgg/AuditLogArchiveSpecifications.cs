﻿using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Extensions;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Data.Entity.SqlServer;

namespace Domain.MainBoundedContext.Aggregates.AuditLogArchiveAgg
{
    public static class AuditLogArchiveSpecifications
    {
        public static Specification<AuditLogArchive> DefaultSpec()
        {
            Specification<AuditLogArchive> specification = new TrueSpecification<AuditLogArchive>();

            return specification;
        }

        public static ISpecification<AuditLogArchive> AuditLogArchiveWithDateRangeAndFullText(DateTime startDate, DateTime endDate, string text)
        {
            Specification<AuditLogArchive> specification = new TrueSpecification<AuditLogArchive>();

            if (startDate != null && endDate != null)
            {
                endDate = endDate.Add(DefaultSettings.Instance.EndDateTimeSpan);

                var dateRangeSpec = new DirectSpecification<AuditLogArchive>(x => x.EventDate >= startDate && x.EventDate <= endDate);

                specification &= dateRangeSpec;

                if (!String.IsNullOrWhiteSpace(text))
                {
                    text = text.SanitizePatIndexInput();

                    var tableNameSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.TableName) > 0);
                    var recordIDSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.RecordID) > 0);
                    var additionalNarrationSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.AdditionalNarration) > 0);

                    var applicationUserNameSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.ApplicationUserName) > 0);
                    var environmentUserNameSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentUserName) > 0);
                    var environmentMachineNameSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentMachineName) > 0);
                    var environmentOSVersionSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentOSVersion) > 0);
                    var environmentMACAddressSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentMACAddress) > 0);
                    var environmentMotherboardSerialNumberSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentMotherboardSerialNumber) > 0);
                    var environmentProcessorIdSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentProcessorId) > 0);
                    var environmentIPAddressSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentIPAddress) > 0);

                    specification &= (tableNameSpec | recordIDSpec | additionalNarrationSpec |
                        applicationUserNameSpec | environmentUserNameSpec | environmentMachineNameSpec | environmentOSVersionSpec | environmentMACAddressSpec | environmentMotherboardSerialNumberSpec | environmentProcessorIdSpec | environmentIPAddressSpec);
                }
            }

            return specification;
        }

        public static Specification<AuditLogArchive> AuditLogArchiveFullText(string text)
        {
            Specification<AuditLogArchive> specification = new TrueSpecification<AuditLogArchive>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                text = text.SanitizePatIndexInput();

                var tableNameSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.TableName) > 0);
                var recordIDSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.RecordID) > 0);
                var additionalNarrationSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.AdditionalNarration) > 0);

                var applicationUserNameSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.ApplicationUserName) > 0);
                var environmentUserNameSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentUserName) > 0);
                var environmentMachineNameSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentMachineName) > 0);
                var environmentOSVersionSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentOSVersion) > 0);
                var environmentMACAddressSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentMACAddress) > 0);
                var environmentMotherboardSerialNumberSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentMotherboardSerialNumber) > 0);
                var environmentProcessorIdSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentProcessorId) > 0);
                var environmentIPAddressSpec = new DirectSpecification<AuditLogArchive>(c => SqlFunctions.PatIndex(text, c.EnvironmentIPAddress) > 0);

                specification &= (tableNameSpec | recordIDSpec | additionalNarrationSpec |
                    applicationUserNameSpec | environmentUserNameSpec | environmentMachineNameSpec | environmentOSVersionSpec | environmentMACAddressSpec | environmentMotherboardSerialNumberSpec | environmentProcessorIdSpec | environmentIPAddressSpec);
            }

            return specification;
        }
    }
}
