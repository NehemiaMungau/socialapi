﻿using Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.Aggregates.EnumerationAgg
{
    public class Enumeration : Entity
    {
        public string Key { get; set; }

        public int Value { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
