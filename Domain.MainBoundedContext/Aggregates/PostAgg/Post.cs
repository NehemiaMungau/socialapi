﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.Aggregates.PostAgg
{
    public class Post: Entity
    {
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
