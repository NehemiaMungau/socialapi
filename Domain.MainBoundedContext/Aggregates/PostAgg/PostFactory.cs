﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.Aggregates.PostAgg
{
    public static class PostFactory
    {
        public static Post CreatePost(string createdBy)
        {
            Post post = new Post();

            post.GenerateNewIdentity();
            
            post.CreatedBy = createdBy;

            post.CreatedDate = DateTime.Now;

            return post;
        }
    }
}
