﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class Duration : ValueObject<Duration>
    {
        public DateTime StartDate { get; private set; }

        public DateTime EndDate { get; private set; }

        public Duration(DateTime startDate, DateTime endDate)
        {
            if (endDate >= startDate)
            {
                this.StartDate = startDate;
                this.EndDate = endDate;
            }
            else
            {
                this.StartDate = startDate;
                this.EndDate = startDate;
            }
        }

        private Duration()
        {

        }
    }
}
