﻿using Domain.MainBoundedContext.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg
{
    public static class EmailAlertFactory
    {
        public static EmailAlert CreateEmailAlert(MailMessage mailMessage)
        {
            var emailAlert = new EmailAlert();

            emailAlert.GenerateNewIdentity();

            emailAlert.MailMessage = mailMessage;

            emailAlert.CreatedDate = DateTime.UtcNow;

            return emailAlert;
        }
    }
}
