﻿using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg
{
    public class EmailAlert : Entity
    {
        public virtual MailMessage MailMessage { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
