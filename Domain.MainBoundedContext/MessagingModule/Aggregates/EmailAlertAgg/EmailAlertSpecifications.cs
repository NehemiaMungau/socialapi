﻿using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Data.Entity.SqlServer;

namespace Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg
{
    public static class EmailAlertSpecifications
    {
        public static Specification<EmailAlert> DefaultSpec()
        {
            Specification<EmailAlert> specification = new TrueSpecification<EmailAlert>();

            return specification;
        }

        public static ISpecification<EmailAlert> EmailAlertWithDLRStatus(int dlrStatus)
        {
            Specification<EmailAlert> specification = new TrueSpecification<EmailAlert>();

            specification &= new DirectSpecification<EmailAlert>(x => x.MailMessage.DLRStatus == dlrStatus);

            return specification;
        }

        public static ISpecification<EmailAlert> EmailAlertWithDLRStatusAndOrigin(int dlrStatus, int origin)
        {
            Specification<EmailAlert> specification = new TrueSpecification<EmailAlert>();

            specification &= new DirectSpecification<EmailAlert>(x => x.MailMessage.DLRStatus == dlrStatus && x.MailMessage.Origin == origin);

            return specification;
        }

        public static Specification<EmailAlert> EmailAlertFullText(int dlrStatus, string text)
        {
            Specification<EmailAlert> specification = new DirectSpecification<EmailAlert>(x => x.MailMessage.DLRStatus == dlrStatus);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var toSpec = new DirectSpecification<EmailAlert>(c => SqlFunctions.PatIndex(text, c.MailMessage.To) > 0);
                var subjectSpec = new DirectSpecification<EmailAlert>(c => SqlFunctions.PatIndex(text, c.MailMessage.Subject) > 0);
                var bodySpec = new DirectSpecification<EmailAlert>(c => SqlFunctions.PatIndex(text, c.MailMessage.Body) > 0);

                specification &= (toSpec | subjectSpec | bodySpec);
            }

            return specification;
        }

        public static Specification<EmailAlert> EmailAlertFullText(int dlrStatus, DateTime startDate, DateTime endDate, string text)
        {
            endDate = endDate.Add(DefaultSettings.Instance.EndDateTimeSpan);

            Specification<EmailAlert> specification = new DirectSpecification<EmailAlert>(x => x.MailMessage.DLRStatus == dlrStatus && x.CreatedDate >= startDate && x.CreatedDate <= endDate);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var toSpec = new DirectSpecification<EmailAlert>(c => SqlFunctions.PatIndex(text, c.MailMessage.To) > 0);
                var subjectSpec = new DirectSpecification<EmailAlert>(c => SqlFunctions.PatIndex(text, c.MailMessage.Subject) > 0);
                var bodySpec = new DirectSpecification<EmailAlert>(c => SqlFunctions.PatIndex(text, c.MailMessage.Body) > 0);

                specification &= (toSpec | subjectSpec | bodySpec);
            }

            return specification;
        }
    }
}
