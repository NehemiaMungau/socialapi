﻿using Domain.MainBoundedContext.Aggregates.EnumerationAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping
{
    class EnumerationEntityConfiguration : EntityTypeConfiguration<Enumeration>
    {
        public EnumerationEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Key).HasMaxLength(256);

            Property(x => x.Description).HasMaxLength(256);

            ToTable(string.Format("{0}Enumerations", DefaultSettings.Instance.TablePrefix));
        }
    }
}
