﻿using Domain.MainBoundedContext.Aggregates.LikeAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping
{
    class LikeEntityConfiguration: EntityTypeConfiguration<Like>
    {
        public LikeEntityConfiguration()
        {
            HasKey(x => x.Id);

            ToTable(string.Format("{0}Likes", DefaultSettings.Instance.TablePrefix));
        }
    }
}
