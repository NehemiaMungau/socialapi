﻿using Domain.MainBoundedContext.Aggregates.CommentAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping
{
    class CommentEntityConfiguration : EntityTypeConfiguration<Comment>
    {
        public CommentEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.CreatedBy).HasMaxLength(50);

            Property(x => x.Content).HasMaxLength(250);

            ToTable(string.Format("{0}Comments", DefaultSettings.Instance.TablePrefix));

        }
    }
}
