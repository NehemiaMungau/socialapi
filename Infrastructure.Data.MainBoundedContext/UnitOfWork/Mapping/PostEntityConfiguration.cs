﻿using Domain.MainBoundedContext.Aggregates.PostAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping
{
    class PostEntityConfiguration : EntityTypeConfiguration<Post>
    {
        public PostEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.CreatedBy).HasMaxLength(50);
            
            ToTable(string.Format("{0}Posts", DefaultSettings.Instance.TablePrefix));
        }
    }
}
