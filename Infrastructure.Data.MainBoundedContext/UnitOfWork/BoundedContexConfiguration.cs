﻿using Infrastructure.Data.FFIBoundedContext.Migrations;
using System.Data.Entity;

namespace Infrastructure.Data.FFIBoundedContext.UnitOfWork
{
    public class BoundedContexConfiguration : DbConfiguration
    {
        public BoundedContexConfiguration()
        {
            SetMigrationSqlGenerator("System.Data.SqlClient", () => new NonClusteredPrimaryKeySqlMigrationSqlGenerator());
        }
    }
}
