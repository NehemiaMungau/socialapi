﻿using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.ExpressionTreeSerialization;
using Infrastructure.Crosscutting.Framework.Utils;
using Infrastructure.Data.MainBoundedContext.UnitOfWork;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainBoundedContext.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : Entity
    {
        private readonly IAmbientDbContextLocator _ambientDbContextLocator;

        public Repository(IAmbientDbContextLocator ambientDbContextLocator)
        {
            if (ambientDbContextLocator == null)
                throw new ArgumentNullException("ambientDbContextLocator");

            _ambientDbContextLocator = ambientDbContextLocator;
        }

        public TEntity Get(Guid entityId, ServiceHeader serviceHeader)
        {
            return GetDbContext(serviceHeader).Set<TEntity>().Find(entityId);
        }

        public Task<TEntity> GetAsync(Guid entityId, ServiceHeader serviceHeader)
        {
            return GetDbContext(serviceHeader).Set<TEntity>().FindAsync(entityId);
        }

        public void Add(TEntity entity, ServiceHeader serviceHeader)
        {
            GetDbContext(serviceHeader).Set<TEntity>().Add(entity);
        }

        public void Merge(TEntity original, TEntity current, ServiceHeader serviceHeader)
        {
            GetDbContext(serviceHeader).Entry(original).CurrentValues.SetValues(current);
        }

        public void Remove(TEntity entity, ServiceHeader serviceHeader)
        {
            GetDbContext(serviceHeader).Set<TEntity>().Remove(entity);
        }

        public IEnumerable<TEntity> GetAll(ServiceHeader serviceHeader)
        {
            return GetDbContext(serviceHeader).Set<TEntity>();
        }

        public IEnumerable<TEntity> AllMatching(ISpecification<TEntity> specification, ServiceHeader serviceHeader, params Expression<Func<TEntity, object>>[] paths)
        {
            IQueryable<TEntity> items = null;

            var objectSet = GetDbContext(serviceHeader).Set<TEntity>();

            if (paths != null && paths.Any())
            {
                IQueryable<TEntity> entitySet = null;

                Array.ForEach(paths, path =>
                {
                    entitySet = (entitySet ?? objectSet).Include(path);
                });

                items = entitySet.Where(specification.SatisfiedBy());
            }
            else items = objectSet.Where(specification.SatisfiedBy());

            return items;
        }

        public PagedCollection<TEntity> AllMatchingPaged(ISpecification<TEntity> specification, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader, params Expression<Func<TEntity, object>>[] paths)
        {
            IQueryable<TEntity> items = null;

            var totalItems = 0;

            var objectSet = GetDbContext(serviceHeader).Set<TEntity>();

            if (paths != null && paths.Any())
            {
                IQueryable<TEntity> entitySet = null;

                Array.ForEach(paths, path =>
                {
                    entitySet = (entitySet ?? objectSet).Include(path);
                });

                items = entitySet.Where(specification.SatisfiedBy());
            }
            else items = objectSet.Where(specification.SatisfiedBy());

            totalItems = items.Count();

            if (pageSize != 0)
            {
                if (sortFields != null && sortFields.Any())
                {
                    sortFields.ForEach(s => items = ExpressionTreeSerializationUtils.CallOrderBy(items, s, ascending));

                    items = items.Skip(pageSize * pageIndex); // NB: orderby must be called before skip(..)
                }

                items = items.Take(pageSize);
            }

            return new PagedCollection<TEntity> { PageIndex = pageIndex, PageSize = pageSize, Items = items.AsEnumerable(), TotalItems = totalItems };
        }

        public int AllMatchingCount(ISpecification<TEntity> specification, ServiceHeader serviceHeader)
        {
            IQueryable<TEntity> items = null;

            var objectSet = GetDbContext(serviceHeader).Set<TEntity>();

            items = objectSet.Where(specification.SatisfiedBy());

            return items != null ? items.Count() : 0;
        }

        public IEnumerable<TEntity> DbSetSqlQuery(string sql, ServiceHeader serviceHeader, params object[] parameters)
        {
            return GetDbContext(serviceHeader).Set<TEntity>().SqlQuery(sql, parameters);
        }

        public IEnumerable<TElement> DatabaseSqlQuery<TElement>(string sql, ServiceHeader serviceHeader, params object[] parameters)
        {
            return GetDbContext(serviceHeader).Database.SqlQuery<TElement>(sql, parameters);
        }

        public int DatabaseExecuteSqlCommand(string sql, ServiceHeader serviceHeader, params object[] parameters)
        {
            return GetDbContext(serviceHeader).Database.ExecuteSqlCommand(sql, parameters);
        }

        private BoundedContextUnitOfWork GetDbContext(ServiceHeader serviceHeader)
        {
            var dbContext = _ambientDbContextLocator.Get<BoundedContextUnitOfWork>(serviceHeader);

            if (dbContext == null)
                throw new InvalidOperationException("No ambient DbContext of type BoundedContextUnitOfWork found. This means that this repository method has been called outside of the scope of a DbContextScope. A repository must only be accessed within the scope of a DbContextScope, which takes care of creating the DbContext instances that the repositories need and making them available as ambient contexts. This is what ensures that, for any given DbContext-derived type, the same instance is used throughout the duration of a business transaction. To fix this issue, use IDbContextScopeFactory in your top-level business logic service method to create a DbContextScope that wraps the entire business transaction that your service method implements. Then access this repository within that scope. Refer to the comments in the IDbContextScope.cs file for more details.");

            return dbContext;
        }
    }
}
