namespace Infrastructure.Data.MainBoundedContext.Migrations
{
    using FFIBoundedContext.Migrations;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<Infrastructure.Data.MainBoundedContext.UnitOfWork.BoundedContextUnitOfWork>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            CommandTimeout = 3600;
            CodeGenerator = new NonClusteredPrimaryKeyCSharpMigrationCodeGenerator();
            SetSqlGenerator("System.Data.SqlClient", new NonClusteredPrimaryKeySqlMigrationSqlGenerator());
        }

        protected override void Seed(Infrastructure.Data.MainBoundedContext.UnitOfWork.BoundedContextUnitOfWork context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }

    public sealed class AutoConfiguration : MigrateDatabaseToLatestVersion<Infrastructure.Data.MainBoundedContext.UnitOfWork.BoundedContextUnitOfWork, Configuration>
    {
        public AutoConfiguration(bool useSuppliedContext)
            : base(useSuppliedContext)
        { }

        public AutoConfiguration(string connectionStringName)
            : base(connectionStringName)
        { }
    }
}
