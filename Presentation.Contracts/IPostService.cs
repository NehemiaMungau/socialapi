﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts
{
    [ServiceContract(Name = "IPostService")]
    public interface IPostService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewPost(PostDTO postDTO, AsyncCallback callback, Object state);
        PostDTO EndAddNewPost(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewComment(CommentDTO postDTO, AsyncCallback callback, Object state);
        CommentDTO EndAddNewComment(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewLikeDTO(LikeDTO likeDTO, AsyncCallback callback, Object state);
        LikeDTO EndAddNewLikeDTO(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindPostById(Guid postId, AsyncCallback callback, Object state);
        PostDTO EndFindPostById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindCommentById(Guid commentId, AsyncCallback callback, Object state);
        CommentDTO EndFindCommentById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindLikeDTOById(Guid likeId, AsyncCallback callback, Object state);
        LikeDTO EndFindLikeDTOById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindPosts(AsyncCallback callback, Object state);
        List<PostDTO> EndFindPosts(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindComments(Guid id,AsyncCallback callback, Object state);
        List<CommentDTO> EndFindComments(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindLikes(Guid id, AsyncCallback callback, Object state);
        List<LikeDTO> EndFindLikes(IAsyncResult result);
    }
}
