﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts
{
    [ServiceContract(Name = "IAuditLogService")]
    public interface IAuditLogService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddAuditLog(AuditLogDTO auditLogDTO, AsyncCallback callback, Object state);
        AuditLogDTO EndAddAuditLog(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindAuditLog(Guid auditLogId, AsyncCallback callback, Object state);
        AuditLogDTO EndFindAuditLog(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindAuditLogs(AsyncCallback callback, Object state);
        List<AuditLogDTO> EndFindAuditLogs(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindAuditLogsByDateRangeAndFilterInPage(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string filter, AsyncCallback callback, Object state);
        PageCollectionInfo<AuditLogDTO> EndFindAuditLogsByDateRangeAndFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindAuditLogsByFilter(string filter, AsyncCallback callback, Object state);
        List<AuditLogDTO> EndFindAuditLogsByFilter(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindAuditLogsInPage(int pageIndex, int pageSize, AsyncCallback callback, Object state);
        PageCollectionInfo<AuditLogDTO> EndFindAuditLogsInPage(IAsyncResult result);
    }
}
