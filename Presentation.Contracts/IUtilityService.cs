﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.ServiceModel;

namespace Presentation.Contracts
{
    [ServiceContract(Name = "IUtilityService")]
    public interface IUtilityService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindApplicationDomainsByFilterInPage(string text, int pageIndex, int pageSize, AsyncCallback callback, Object state);
        PageCollectionInfo<ApplicationDomainWrapper> EndFindApplicationDomainsByFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginConfigureApplicationDatabase(AsyncCallback callback, Object state);
        bool EndConfigureApplicationDatabase(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginSeedEnumerations(AsyncCallback callback, Object state);
        bool EndSeedEnumerations(IAsyncResult result);
    }
}
