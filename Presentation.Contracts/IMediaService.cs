﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.ServiceModel;

namespace Presentation.Contracts
{
    [ServiceContract(Name = "IMediaService")]
    public interface IMediaService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginMediaUpload(FileData fileData, AsyncCallback callback, Object state);
        string EndMediaUpload(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginMediaUploadDone(string filename, AsyncCallback callback, Object state);
        bool EndMediaUploadDone(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetMedia(Guid sku, AsyncCallback callback, Object state);
        MediaDTO EndGetMedia(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginPostFile(MediaDTO mediaDTO, AsyncCallback callback, Object state);
        bool EndPostFile(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginPostImage(MediaDTO mediaDTO, AsyncCallback callback, Object state);
        bool EndPostImage(IAsyncResult result);
    }
}
