﻿using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Seedwork
{
    public interface IRepository<TEntity>
        where TEntity : Entity
    {
        TEntity Get(Guid entityId, ServiceHeader serviceHeader);

        Task<TEntity> GetAsync(Guid entityId, ServiceHeader serviceHeader);

        void Add(TEntity entity, ServiceHeader serviceHeader);

        void Merge(TEntity original, TEntity current, ServiceHeader serviceHeader);

        void Remove(TEntity entity, ServiceHeader serviceHeader);

        IEnumerable<TEntity> GetAll(ServiceHeader serviceHeader);

        IEnumerable<TEntity> AllMatching(ISpecification<TEntity> specification, ServiceHeader serviceHeader, params Expression<Func<TEntity, object>>[] paths);

        PagedCollection<TEntity> AllMatchingPaged(ISpecification<TEntity> specification, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader, params Expression<Func<TEntity, object>>[] paths);

        int AllMatchingCount(ISpecification<TEntity> specification, ServiceHeader serviceHeader);

        IEnumerable<TEntity> DbSetSqlQuery(string sql, ServiceHeader serviceHeader, params object[] parameters);

        IEnumerable<TElement> DatabaseSqlQuery<TElement>(string sql, ServiceHeader serviceHeader, params object[] parameters);

        int DatabaseExecuteSqlCommand(string sql, ServiceHeader serviceHeader, params object[] parameters);
    }
}
