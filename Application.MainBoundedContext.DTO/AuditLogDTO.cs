﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO
{
    public class AuditLogDTO : BindingModelBase<AuditLogDTO>
    {
        public AuditLogDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [DataMember]
        [Display(Name = "Event Type")]
        public int EventType { get; set; }

        [DataMember]
        [Display(Name = "Event Type")]
        public string EventTypeDescription
        {
            get
            {
                return Enum.IsDefined(typeof(AuditLogEventType), EventType) ? EnumHelper.GetDescription((AuditLogEventType)EventType) : string.Empty;
            }
        }

        [DataMember]
        [Display(Name = "Table Name")]
        public string TableName { get; set; }

        [DataMember]
        [Display(Name = "Record Id")]
        public string RecordID { get; set; }

        [DataMember]
        [Display(Name = "Narration")]
        public string AdditionalNarration { get; set; }

        [DataMember]
        [Display(Name = "Application User Name")]
        public string ApplicationUserName { get; set; }

        [DataMember]
        [Display(Name = "Environment User Name")]
        public string EnvironmentUserName { get; set; }

        [DataMember]
        [Display(Name = "Environment Machine Name")]
        public string EnvironmentMachineName { get; set; }

        [DataMember]
        [Display(Name = "Environment Domain Name")]
        public string EnvironmentDomainName { get; set; }

        [DataMember]
        [Display(Name = "Environment Operating System Version")]
        public string EnvironmentOSVersion { get; set; }

        [DataMember]
        [Display(Name = "Environment MAC Address")]
        public string EnvironmentMACAddress { get; set; }

        [DataMember]
        [Display(Name = "Environment Motherboard Serial Number")]
        public string EnvironmentMotherboardSerialNumber { get; set; }

        [DataMember]
        [Display(Name = "Environment Processor Id")]
        public string EnvironmentProcessorId { get; set; }

        [DataMember]
        [Display(Name = "Environment IP Address")]
        public string EnvironmentIPAddress { get; set; }

        [DataMember]
        [Display(Name = "Event Date")]
        public DateTime EventDate { get; set; }
    }
}
