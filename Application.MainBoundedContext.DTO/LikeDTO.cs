﻿using Application.Seedwork;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO
{
    public class LikeDTO : BindingModelBase<LikeDTO>
    {
        public LikeDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        [Display(Name = "Id")]
        public Guid Id { get; set; }
        
        [DataMember]
        [Display(Name = "PostId")]
        public Guid PostId { get; set; }

        [DataMember]
        [Display(Name = "PostCreatedBy")]
        public string PostCreatedBy { get; set; }

        [DataMember]
        [Display(Name = "PostCreatedDate")]
        public DateTime PostCreatedDate { get; set; }

        [DataMember]
        [Display(Name = "CreatedBy")]
        public string CreatedBy { get; set; }


        [DataMember]
        [Display(Name = "CreatedDate")]
        public DateTime CreatedDate { get; set; }
    }
}
