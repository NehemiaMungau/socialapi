﻿using Application.Seedwork;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO
{
    public class CommentDTO : BindingModelBase<CommentDTO>
    {
        public CommentDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [DataMember]
        [Display(Name = "PostId")]
        public Guid PostId { get; set; }

        [DataMember]
        [Display(Name = "Content")]
        public string Content { get; set; }

        [DataMember]
        [Display(Name = "CreatedBy")]
        public string CreatedBy { get; set; }

        [DataMember]
        [Display(Name = "CreatedDate")]
        public DateTime CreatedDate { get; set; }
    }
}
