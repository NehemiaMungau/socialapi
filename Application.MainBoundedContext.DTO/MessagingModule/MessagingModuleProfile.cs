﻿using AutoMapper;
using Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg;

namespace Application.MainBoundedContext.DTO.MessagingModule
{
    public class MessagingModuleProfile : Profile
    {
        protected override void Configure()
        {
            //EmailAlert => EmailAlertDTO
            CreateMap<EmailAlert, EmailAlertDTO>()
                .ForMember(dest => dest.MaskedMailMessageBody, opt => opt.MapFrom(src => src.MailMessage.SecurityCritical ? "Security Critical" : src.MailMessage.Body))
                .ForMember(dest => dest.MailMessageDLRStatusDescription, opt => opt.Ignore())
                .ForMember(dest => dest.MailMessageOriginDescription, opt => opt.Ignore());
        }
    }
}
