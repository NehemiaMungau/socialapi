﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SelfHostedWebApi
{
    public class CommentController : ApiController
    {
        private readonly IChannelService _channelService;

        private ServiceHeader serviceHeader;

        public CommentController()
        {
            _channelService = new ChannelService();

            serviceHeader = new ServiceHeader();

            serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            serviceHeader.ApplicationDomainName = "DEV_FSA";
        }

        public async Task<HttpResponseMessage> Post(CommentDTO commentDTO)
        {
            var response = new HttpResponseMessage();


            try
            {
                var create = await _channelService.AddNewCommentAsync(commentDTO, serviceHeader);

                if (create != null)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                    response.Content = new StringContent("post created sucessful.");
                    response.ReasonPhrase = "SUCCESS.";
                    return response;
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                    response.Content = new StringContent("post was not created");
                    response.ReasonPhrase = "Failed.";

                    return response;
                }

            }
            catch (Exception ex)
            {
                var error = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error has occurred."),
                    ReasonPhrase = "Failed."
                };


                throw new HttpResponseException(error);
            }
        }

        [HttpGet]
        public async Task<IEnumerable<CommentDTO>> GetComments(Guid postId)
        {

            var serviceHeader = new ServiceHeader();

            var comments = await _channelService.FindCommentsAsync(postId, serviceHeader);

            if (comments != null)
            {
                return comments;
            }
            else
            {
                return new List<CommentDTO>();
            }

        }
    }
}
