﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Web.Http;

namespace SelfHostedWebApi
{
    public class PostController : ApiController
    {
        private readonly IChannelService _channelService;

        private ServiceHeader serviceHeader;

        public PostController()
        {
            _channelService = new ChannelService();

            serviceHeader = new ServiceHeader();

            serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            serviceHeader.ApplicationDomainName = "DEV_FSA";
        }

        public async Task<HttpResponseMessage> Post(PostDTO postDTO)
        {
            var response = new HttpResponseMessage();
           

            try
            {
                var create =await _channelService.AddNewPostAsync(postDTO,serviceHeader);

                if (create != null)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                    response.Content = new StringContent("post created sucessful.");
                    response.ReasonPhrase = "SUCCESS.";
                    return response;
                }else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                    response.Content = new StringContent("post was not created");
                    response.ReasonPhrase = "Failed.";

                    return response;
                }

            }
            catch (Exception ex)
            {
                var error = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error has occurred."),
                    ReasonPhrase = "Failed."
                };


                throw new HttpResponseException(error);
            }
        }

        [HttpGet]
        public async Task<IEnumerable<PostDTO>>  Get()
        {
          
            var serviceHeader = new ServiceHeader();

            var posts = await _channelService.FindPostsAsync(serviceHeader);

            if (posts != null)
            {
                return posts;
            }else
            {
                return new List<PostDTO>();
            }
           
        }

        [HttpGet]
        public async Task<PostDTO> Get(Guid id)
        {
            var serviceHeader = new ServiceHeader();

            var post = await _channelService.FindPostByIdAsync(id, serviceHeader);

            if (post!= null){

                return post;
            }

            else
            {
                return new PostDTO();
            }
        }
    }
}
