﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SelfHostedWebApi
{
    public class LikeController : ApiController
    {
        private readonly IChannelService _channelService;

        private ServiceHeader serviceHeader;

        public LikeController()
        {
            _channelService = new ChannelService();

            serviceHeader = new ServiceHeader();

            serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            serviceHeader.ApplicationDomainName = "DEV_FSA";
        }

        public async Task<HttpResponseMessage> Post(LikeDTO likeDTO)
        {
            var response = new HttpResponseMessage();


            try
            {
                var create = await _channelService.AddNewLikeDTOAsync(likeDTO, serviceHeader);

                if (create != null)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                    response.Content = new StringContent("post created sucessful.");
                    response.ReasonPhrase = "SUCCESS.";
                    return response;
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                    response.Content = new StringContent("post was not created");
                    response.ReasonPhrase = "Failed.";

                    return response;
                }

            }
            catch (Exception ex)
            {
                var error = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error has occurred."),
                    ReasonPhrase = "Failed."
                };


                throw new HttpResponseException(error);
            }
        }

        [HttpGet]
        public async Task<IEnumerable<LikeDTO>> GetLikes(Guid postId)
        {

            var serviceHeader = new ServiceHeader();

            var likes = await _channelService.FindLikesAsync(postId, serviceHeader);

            if (likes != null)
            {
                return likes;
            }
            else
            {
                return new List<LikeDTO>();
            }

        }

        [HttpGet]
        public async Task<LikeDTO> Get(Guid likeId)
        {
            var serviceHeader = new ServiceHeader();

            var post = await _channelService.FindLikeDTOIdAsync(likeId, serviceHeader);

            if (post != null)
            {

                return post;
            }

            else
            {
                return new LikeDTO();
            }

        }
    }
}