﻿
using System;
using Quartz;

namespace Presentation.Infrastructure.Services
{
    public interface IPlugin
    {
        Guid Id { get; }

        string Description { get; }

        //void DoWork(params string[] args);
        void DoWork(IScheduler scheduler, params string[] args);
        void Exit();
        
    }
}
