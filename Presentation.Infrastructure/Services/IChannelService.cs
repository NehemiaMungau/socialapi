﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Presentation.Infrastructure.Services
{
    public interface IChannelService
    {
        #region AuditLogDTO

        Task<AuditLogDTO> AddAuditLogAsync(AuditLogDTO auditLogDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<PageCollectionInfo<AuditLogDTO>> FindAuditLogsInPageAsync(int pageIndex, int pageSize, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<PageCollectionInfo<AuditLogDTO>> FindAuditLogsByDateRangeAndFilterInPageAsync(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string filter, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        #endregion

        #region FileUpload

        Task<string> FileUploadAsync(FileData fileData, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> FileUploadDoneAsync(string filename, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> PingNetworkAsync(string hostNameOrAddress, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        #endregion

        #region MediaDTO

        string GetMediaHostName();

        Task<string> MediaUploadAsync(FileData fileData, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> MediaUploadDoneAsync(string filename, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<MediaDTO> GetMediaAsync(Guid sku, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> PostFileAsync(MediaDTO mediaDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> PostImageAsync(MediaDTO mediaDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        #endregion

        #region ApplicationDomainWrapper

        Task<PageCollectionInfo<ApplicationDomainWrapper>> FindApplicationDomainsByFilterInPageAsync(int pageIndex, int pageSize, string filter, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> ConfigureApplicationDatabaseAsync(ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> SeedEnumerationsAsync(ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        #endregion


        #region EmailAlertDTO

        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsByFilterInPageAsync(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsByDateRangeAndFilterInPageAsync(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsInPageAsync(int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> AddEmailAlertsAsync(ObservableCollection<EmailAlertDTO> emailAlertDTOs, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<EmailAlertDTO> AddEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> UpdateEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<EmailAlertDTO> FindEmailAlertAsync(Guid emailAlertId, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<List<EmailAlertDTO>> FindEmailAlertsByDLRStatusAsync(int dlrStatus, ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        #endregion

        #region Post

        Task<PostDTO> AddNewPostAsync(PostDTO postDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10);

        Task<CommentDTO> AddNewCommentAsync(CommentDTO commentDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10);

        Task<LikeDTO> AddNewLikeDTOAsync(LikeDTO likeDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10);


        Task<PostDTO> FindPostByIdAsync(Guid Id, ServiceHeader serviceHeader, double timeoutMinutes = 10);

        Task<CommentDTO> FindCommentByIdAsync(Guid Id, ServiceHeader serviceHeader, double timeoutMinutes = 10);

        Task<LikeDTO> FindLikeDTOIdAsync(Guid Id, ServiceHeader serviceHeader, double timeoutMinutes = 10);

        Task<List<PostDTO>> FindPostsAsync( ServiceHeader serviceHeader, double timeoutMinutes = 10);

        Task<List<CommentDTO>> FindCommentsAsync(Guid id,ServiceHeader serviceHeader, double timeoutMinutes = 10);

        Task<List<LikeDTO>> FindLikesAsync(Guid id, ServiceHeader serviceHeader, double timeoutMinutes = 10);
        #endregion

      




    }
}
