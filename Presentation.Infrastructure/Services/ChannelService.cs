﻿using Application.MainBoundedContext;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using DistributedServices.Seedwork.EndpointBehaviors;
using DistributedServices.Seedwork.ErrorHandlers;
using Infrastructure.Crosscutting.Framework.Extensions;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Contracts;
using Presentation.Contracts.MessagingModule;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Presentation.Infrastructure.Services
{
    [Export(typeof(IChannelService))]
    public class ChannelService : IChannelService
    {
        private static readonly object SyncRoot = new object();

        private readonly Dictionary<string, object> _channelFactoryHashtable;

        public ChannelService()
        {
            _channelFactoryHashtable = new Dictionary<string, object>();
        }

        #region AuditLogDTO

        public Task<AuditLogDTO> AddAuditLogAsync(AuditLogDTO auditLogDTO, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<AuditLogDTO>();

            IAuditLogService service = GetService<IAuditLogService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    AuditLogDTO response = ((IAuditLogService)result.AsyncState).EndAddAuditLog(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginAddAuditLog(auditLogDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<AuditLogDTO>> FindAuditLogsInPageAsync(int pageIndex, int pageSize, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<AuditLogDTO>>();

            IAuditLogService service = GetService<IAuditLogService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<AuditLogDTO> response = ((IAuditLogService)result.AsyncState).EndFindAuditLogsInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginFindAuditLogsInPage(pageIndex, pageSize, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<AuditLogDTO>> FindAuditLogsByDateRangeAndFilterInPageAsync(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string filter, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<AuditLogDTO>>();

            IAuditLogService service = GetService<IAuditLogService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<AuditLogDTO> response = ((IAuditLogService)result.AsyncState).EndFindAuditLogsByDateRangeAndFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginFindAuditLogsByDateRangeAndFilterInPage(pageIndex, pageSize, startDate, endDate, filter, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region FileUpload

        public Task<string> FileUploadAsync(FileData fileData, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<string>();

            IFileUploadService service = GetService<IFileUploadService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    string response = ((IFileUploadService)result.AsyncState).EndFileUpload(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginFileUpload(fileData, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> FileUploadDoneAsync(string filename, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<bool>();

            IFileUploadService service = GetService<IFileUploadService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IFileUploadService)result.AsyncState).EndFileUploadDone(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(false);
                    });
                }
            });

            service.BeginFileUploadDone(filename, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> PingNetworkAsync(string hostNameOrAddress, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<bool>();

            IFileUploadService service = GetService<IFileUploadService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IFileUploadService)result.AsyncState).EndPingNetwork(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(false);
                    });
                }
            });

            service.BeginPingNetwork(hostNameOrAddress, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region MediaDTO

        public string GetMediaHostName()
        {
            var channelFactory = GetChannelFactory<IMediaService>();

            return channelFactory.Endpoint.Address.Uri.Host;
        }

        public Task<string> MediaUploadAsync(FileData fileData, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<string>();

            IMediaService service = GetService<IMediaService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    string response = ((IMediaService)result.AsyncState).EndMediaUpload(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginMediaUpload(fileData, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> MediaUploadDoneAsync(string filename, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<bool>();

            IMediaService service = GetService<IMediaService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IMediaService)result.AsyncState).EndMediaUploadDone(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(false);
                    });
                }
            });

            service.BeginMediaUploadDone(filename, asyncCallback, service);

            return tcs.Task;
        }

        public Task<MediaDTO> GetMediaAsync(Guid sku, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<MediaDTO>();

            IMediaService service = GetService<IMediaService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    MediaDTO response = ((IMediaService)result.AsyncState).EndGetMedia(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginGetMedia(sku, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> PostFileAsync(MediaDTO mediaDTO, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<bool>();

            IMediaService service = GetService<IMediaService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IMediaService)result.AsyncState).EndPostFile(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(false);
                    });
                }
            });

            service.BeginPostFile(mediaDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> PostImageAsync(MediaDTO mediaDTO, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<bool>();

            IMediaService service = GetService<IMediaService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IMediaService)result.AsyncState).EndPostImage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(false);
                    });
                }
            });

            service.BeginPostImage(mediaDTO, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region ApplicationDomainWrapper

        public Task<PageCollectionInfo<ApplicationDomainWrapper>> FindApplicationDomainsByFilterInPageAsync(int pageIndex, int pageSize, string filter, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<ApplicationDomainWrapper>>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<ApplicationDomainWrapper> response = ((IUtilityService)result.AsyncState).EndFindApplicationDomainsByFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginFindApplicationDomainsByFilterInPage(filter, pageIndex, pageSize, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> ConfigureApplicationDatabaseAsync(ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<bool>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IUtilityService)result.AsyncState).EndConfigureApplicationDatabase(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(false);
                    });
                }
            });

            service.BeginConfigureApplicationDatabase(asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> SeedEnumerationsAsync(ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<bool>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IUtilityService)result.AsyncState).EndSeedEnumerations(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(false);
                    });
                }
            });

            service.BeginSeedEnumerations(asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region EmailAlertDTO

        public Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsByFilterInPageAsync(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<EmailAlertDTO>>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<EmailAlertDTO> response = ((IEmailAlertService)result.AsyncState).EndFindEmailAlertsByFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginFindEmailAlertsByFilterInPage(dlrStatus, text, pageIndex, pageSize, sortFields, ascending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsByDateRangeAndFilterInPageAsync(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<EmailAlertDTO>>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<EmailAlertDTO> response = ((IEmailAlertService)result.AsyncState).EndFindEmailAlertsByDateRangeAndFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginFindEmailAlertsByDateRangeAndFilterInPage(dlrStatus, startDate, endDate, text, pageIndex, pageSize, sortFields, ascending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsInPageAsync(int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<EmailAlertDTO>>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<EmailAlertDTO> response = ((IEmailAlertService)result.AsyncState).EndFindEmailAlertsInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginFindEmailAlertsInPage(pageIndex, pageSize, sortFields, ascending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> AddEmailAlertsAsync(ObservableCollection<EmailAlertDTO> emailAlertDTOs, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<bool>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IEmailAlertService)result.AsyncState).EndAddEmailAlerts(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(false);
                    });
                }
            });

            service.BeginAddEmailAlerts(emailAlertDTOs.ExtendedToList(), asyncCallback, service);

            return tcs.Task;
        }

        public Task<EmailAlertDTO> AddEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<EmailAlertDTO>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    EmailAlertDTO response = ((IEmailAlertService)result.AsyncState).EndAddEmailAlert(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginAddEmailAlert(emailAlertDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<bool>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IEmailAlertService)result.AsyncState).EndUpdateEmailAlert(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(false);
                    });
                }
            });

            service.BeginUpdateEmailAlert(emailAlertDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<EmailAlertDTO> FindEmailAlertAsync(Guid emailAlertId, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<EmailAlertDTO>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    EmailAlertDTO response = ((IEmailAlertService)result.AsyncState).EndFindEmailAlert(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginFindEmailAlert(emailAlertId, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<EmailAlertDTO>> FindEmailAlertsByDLRStatusAsync(int dlrStatus, ServiceHeader serviceHeader, double timeoutMinutes)
        {
            var tcs = new TaskCompletionSource<List<EmailAlertDTO>>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    List<EmailAlertDTO> response = ((IEmailAlertService)result.AsyncState).EndFindEmailAlertsByDLRStatus(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });

            service.BeginFindEmailAlertsByDLRStatus(dlrStatus, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

     

        #region Post
        public Task<PostDTO> AddNewPostAsync(PostDTO postDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            var tcs = new TaskCompletionSource<PostDTO>();

            IPostService service = GetService<IPostService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PostDTO response = ((IPostService)result.AsyncState).EndAddNewPost(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });
            service.BeginAddNewPost(postDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<CommentDTO> AddNewCommentAsync(CommentDTO commentDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            var tcs = new TaskCompletionSource<CommentDTO>();

            IPostService service = GetService<IPostService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    CommentDTO response = ((IPostService)result.AsyncState).EndAddNewComment(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });
            service.BeginAddNewComment(commentDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<LikeDTO> AddNewLikeDTOAsync(LikeDTO likeDTO, ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            var tcs = new TaskCompletionSource<LikeDTO>();

            IPostService service = GetService<IPostService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    LikeDTO response = ((IPostService)result.AsyncState).EndAddNewLikeDTO(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });
            service.BeginAddNewLikeDTO(likeDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PostDTO> FindPostByIdAsync(Guid Id, ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            var tcs = new TaskCompletionSource<PostDTO>();

            IPostService service = GetService<IPostService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PostDTO response = ((IPostService)result.AsyncState).EndFindPostById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });
            service.BeginFindPostById(Id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<CommentDTO> FindCommentByIdAsync(Guid Id, ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            var tcs = new TaskCompletionSource<CommentDTO>();

            IPostService service = GetService<IPostService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    CommentDTO response = ((IPostService)result.AsyncState).EndFindCommentById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });
            service.BeginFindCommentById(Id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<LikeDTO> FindLikeDTOIdAsync(Guid Id, ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            var tcs = new TaskCompletionSource<LikeDTO>();

            IPostService service = GetService<IPostService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    LikeDTO response = ((IPostService)result.AsyncState).EndFindLikeDTOById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });
            service.BeginFindLikeDTOById(Id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<PostDTO>> FindPostsAsync(ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            var tcs = new TaskCompletionSource<List<PostDTO>>();

            IPostService service = GetService<IPostService>(serviceHeader, timeoutMinutes);
        
            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    List<PostDTO> response = ((IPostService)result.AsyncState).EndFindPosts(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });
            service.BeginFindPosts(asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<CommentDTO>> FindCommentsAsync(Guid id, ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            var tcs = new TaskCompletionSource<List<CommentDTO>>();

            IPostService service = GetService<IPostService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    List<CommentDTO> response = ((IPostService)result.AsyncState).EndFindComments(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });
            service.BeginFindComments(id,asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<LikeDTO>> FindLikesAsync(Guid id, ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            var tcs = new TaskCompletionSource<List<LikeDTO>>();

            IPostService service = GetService<IPostService>(serviceHeader, timeoutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    List<LikeDTO> response = ((IPostService)result.AsyncState).EndFindLikes(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        tcs.TrySetResult(null);
                    });
                }
            });
            service.BeginFindLikes(id, asyncCallback, service);

            return tcs.Task;
        }
        #endregion

        private TChannel GetService<TChannel>(ServiceHeader serviceHeader, double timeoutMinutes = 10d)
        {
            lock (SyncRoot)
            {
                var endpointConfigurationName = string.Format("{0}", typeof(TChannel));

                ChannelFactory<TChannel> factory = null;

                if (_channelFactoryHashtable.ContainsKey(endpointConfigurationName))
                {
                    factory = (ChannelFactory<TChannel>)_channelFactoryHashtable[endpointConfigurationName];
                }
                else
                {
                    var configurationName = endpointConfigurationName.Substring(endpointConfigurationName.LastIndexOf('.') + 1);

                    factory = new ChannelFactory<TChannel>(configurationName);

                    _channelFactoryHashtable.Add(endpointConfigurationName, factory);
                }

                if (factory.Endpoint.Behaviors.Any())
                    factory.Endpoint.Behaviors.Clear();

                factory.Endpoint.Behaviors.Add(new CustomBehavior(serviceHeader));

                var channel = factory.CreateChannel();

                ((IContextChannel)channel).OperationTimeout = TimeSpan.FromMinutes(timeoutMinutes);

                return channel;
            }
        }

        private ChannelFactory<TChannel> GetChannelFactory<TChannel>()
        {
            lock (SyncRoot)
            {
                var endpointConfigurationName = string.Format("{0}", typeof(TChannel));

                ChannelFactory<TChannel> factory = null;

                if (_channelFactoryHashtable.ContainsKey(endpointConfigurationName))
                {
                    factory = (ChannelFactory<TChannel>)_channelFactoryHashtable[endpointConfigurationName];
                }
                else
                {
                    var configurationName = endpointConfigurationName.Substring(endpointConfigurationName.LastIndexOf('.') + 1);

                    factory = new ChannelFactory<TChannel>(configurationName);

                    _channelFactoryHashtable.Add(endpointConfigurationName, factory);
                }

                return factory;
            }
        }

        private void HandleFault(Exception exception, Action<object> callback)
        {
            if (exception is FaultException<ApplicationServiceError>)
            {
                var fault = exception as FaultException<ApplicationServiceError>;

                LoggerFactory.CreateLog().LogError("HandleFault -> {0}", exception, fault.Message);

                callback(null);
            }
            else if (exception is ProtocolException)
            {
                var responseStream = (exception.InnerException as System.Net.WebException).Response.GetResponseStream() as System.IO.MemoryStream;

                if (responseStream != null)
                {
                    var responseBytes = responseStream.ToArray();

                    var responseString = System.Text.Encoding.UTF8.GetString(responseBytes);

                    LoggerFactory.CreateLog().LogError("HandleFault -> {0}", exception, responseString);

                    callback(null);
                }
                else
                {
                    LoggerFactory.CreateLog().LogError("HandleFault", exception);

                    callback(null);
                }
            }
            else
            {
                LoggerFactory.CreateLog().LogError("HandleFault", exception);

                callback(null);
            }
        }

        
    }
}
